import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { switchMap, tap } from "rxjs";
import { ICreateKanbanReq, IGetKanbanRes, IUpdateKanbanStatusReq, Priority, Status } from "src/interface";
import { KanbanApiService } from "./kanban.api.service";
import { FormGroup } from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class KanbanService {
    readonly #kanbanApiService = inject(KanbanApiService)

    readonly #kanbanWaitings: WritableSignal<IGetKanbanRes[]> = signal([]);
    readonly #kanbanProgress: WritableSignal<IGetKanbanRes[]> = signal([]);
    readonly #kanbanReview: WritableSignal<IGetKanbanRes[]> = signal([]);
    readonly #kanbanCompleted: WritableSignal<IGetKanbanRes[]> = signal([]);

    readonly kanbanWaitings = computed(() => this.#kanbanWaitings())
    readonly kanbanProgress = computed(() => this.#kanbanProgress())
    readonly kanbanReview = computed(() => this.#kanbanReview())
    readonly kanbanCompleted = computed(() => this.#kanbanCompleted())

    //? 칸반 생성 
    create(formGroup: FormGroup, projectId: number) {
        const { name, priority, status, inspectors, workers, labels, issues, milestones } = formGroup?.value as {
            name: string;
            priority: Priority;
            status: Status;
            inspectors: any;
            workers: any;
            labels: any;
            issues: any;
            milestones: any;
        };

        const inspectorIds = inspectors.map((res: any) => res.id);
        const workersIds = workers.map((res: any) => res.id);
        const labelIds = labels.map((res: any) => res.id);
        const issueIds = issues.map((res: any) => res.id);
        const milestoneIds = milestones.map((res: any) => res.id);

        return this.#kanbanApiService.create({
            name,
            priority,
            status,
            inspectors: inspectorIds,
            workers: workersIds,
            labels: labelIds,
            issues: issueIds,
            milestones: milestoneIds
        }, projectId).pipe(
            switchMap(() => {
                return this.getKanbans(projectId)
            })
        )
    }

    //? 칸반 조회(상태별)
    getKanbans(projectId: number) {
        return this.#kanbanApiService.getKanbans(projectId).pipe(
            tap((res) => {
                const newWaiting: IGetKanbanRes[] = [];
                const newInProgress: IGetKanbanRes[] = [];
                const newCompleted: IGetKanbanRes[] = [];
                const newReview: IGetKanbanRes[] = [];

                res.forEach((item: IGetKanbanRes) => {
                    switch (item.status) {
                        case Status.WAITING:
                            newWaiting.push(item);
                            break;
                        case Status.COMPLETED:
                            newCompleted.push(item);
                            break;
                        case Status.IN_PROGRESS:
                            newInProgress.push(item);
                            break;
                        case Status.REVIEW:
                            newReview.push(item);
                            break;
                    }
                });

                // 신호를 한 번만 업데이트하여 깜빡임 최소화
                this.#kanbanWaitings.set(newWaiting);
                this.#kanbanProgress.set(newInProgress);
                this.#kanbanCompleted.set(newCompleted);
                this.#kanbanReview.set(newReview);
            })
        )
    }

    //? 칸반 상세조회
    getKanban(projectId: number, kanbanId: number) {
        return this.#kanbanApiService.getKanban(projectId, kanbanId);
    }

    //? 칸반 상태 변경
    updateKanbanStatus(projectId: number, updateStatus: IUpdateKanbanStatusReq) {
        return this.#kanbanApiService.updateStatus(projectId, updateStatus).pipe(
            switchMap(() => {
                return this.getKanbans(projectId)
            })
        )
    }

    //? 칸반 수정
    update(formGroup: FormGroup, projectId: number, kanbanId: number) {
        const { name, priority, status, inspectors, workers, labels, issues, milestones } = formGroup?.value as {
            name: string;
            priority: Priority;
            status: Status;
            inspectors: any;
            workers: any;
            labels: any;
            issues: any;
            milestones: any;
        };

        const inspectorIds = inspectors.map((res: any) => res.id);
        const workersIds = workers.map((res: any) => res.id);
        const labelIds = labels.map((res: any) => res.id);
        const issueIds = issues.map((res: any) => res.id);
        const milestoneIds = milestones.map((res: any) => res.id);

        return this.#kanbanApiService.update({
            name,
            priority,
            status,
            inspectors: inspectorIds,
            workers: workersIds,
            labels: labelIds,
            issues: issueIds,
            milestones: milestoneIds
        }, projectId, kanbanId).pipe(
            switchMap(() => {
                return this.getKanbans(projectId)
            })
        )
    }

    //? 칸반 삭제
    delete(projectId: number, kanbanId: number) {
        return this.#kanbanApiService.delete(projectId, kanbanId).pipe(
            switchMap(() => {
                return this.getKanbans(projectId)
            })
        )
    }
}