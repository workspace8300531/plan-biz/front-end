import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { ICreateKanbanReq, IGetKanbanDetailRes, IGetKanbanRes, IUpdateKanbanStatusReq, Status } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class KanbanApiService {

    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    create(dto: ICreateKanbanReq, projectId: number) {
        return this.#http.post<IGetKanbanRes>(`${this.#baseUrl}/projects/${projectId}/kanbans`, dto);
    }

    getKanbans(projectId: number) {
        return this.#http.get<IGetKanbanRes[]>(`${this.#baseUrl}/projects/${projectId}/kanbans`);
    }

    getKanban(projectId: number, kanbanId: number) {
        return this.#http.get<IGetKanbanDetailRes>(`${this.#baseUrl}/projects/${projectId}/kanbans/${kanbanId}`);
    }

    updateStatus(projectId: number, dto: IUpdateKanbanStatusReq) {
        return this.#http.patch<IGetKanbanRes>(`${this.#baseUrl}/projects/${projectId}/kanbans/status`, dto);
    }

    update(dto: ICreateKanbanReq, projectId: number, kanbanId: number) {
        return this.#http.put<IGetKanbanRes>(`${this.#baseUrl}/projects/${projectId}/kanbans/${kanbanId}`, dto);

    }

    delete(projectId: number, kanbanId: number) {
        return this.#http.delete<IGetKanbanRes>(`${this.#baseUrl}/projects/${projectId}/kanbans/${kanbanId}`);
    }
}