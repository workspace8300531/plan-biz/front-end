import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { switchMap, tap } from "rxjs";
import { IGetIssueRes, Priority } from "src/interface";
import { IGetChannelRes } from "src/interface/channel/channel.response";
import { IssueApiService } from "./issue.api.service";

@Injectable({
    providedIn: 'root'
})
export class IssueService {
    readonly #issueApiService = inject(IssueApiService)

    readonly #issues: WritableSignal<IGetIssueRes[]> = signal([]);
    readonly issues = computed(() => this.#issues())

    //? 이슈 생성
    create(formGroup: FormGroup, channelId: number) {
        const { name, content, priority, status, labels } = formGroup?.value as {
            name: string;
            content: string;
            status: boolean;
            priority: Priority;
            labels: any
        };

        const labelIds = labels.map((res: any) => res.id);

        return this.#issueApiService.create({ name, status, content, priority, labels: labelIds }, channelId).pipe(
            switchMap(() => {
                return this.getIssues(channelId)
            })
        )
    }

    //? 이슈 좋아요
    toggleLike(channelId: number, issueId: number) {
        return this.#issueApiService.toggleLike(channelId, issueId).pipe(
            switchMap(() => {
                return this.getIssues(channelId)
            })
        )
    }

    //? 이슈 댓글
    createComment(formGroup: FormGroup, channelId: number, issueId: number) {
        const { content } = formGroup?.value as {
            content: string;
        };

        return this.#issueApiService.createComment({ content }, channelId, issueId).pipe(
            switchMap(() => {
                return this.getIssues(channelId)
            })
        )
    }

    //? 이슈 조회
    getIssues(channelId: number) {
        return this.#issueApiService.getIssues(channelId).pipe(
            tap((res) => {
                this.#issues.set(res);
            })
        )
    }

    //? 이슈 상세 조회
    getIssue(channelId: number, issueId: number) {
        return this.#issueApiService.getIssue(channelId, issueId)
    }

    //? 이슈 검색
    searchIssues(keyword: string) {
        return this.#issueApiService.searchIssues(keyword).pipe(
            tap((res) => {
                this.#issues.set(res)
            })
        )
    }

    //? 이슈 수정
    update(formGroup: FormGroup, channelId: number, issueId: number) {
        const { name, content, priority, status, labels } = formGroup?.value as {
            name: string;
            content: string;
            status: boolean;
            priority: Priority;
            labels: any
        };

        const labelIds = labels.map((res: any) => res.id);

        return this.#issueApiService.update({ name, status, content, priority, labels: labelIds }, channelId, issueId).pipe(
            switchMap(() => {
                return this.getIssues(channelId)
            })
        )
    }

    //? 이슈 삭제
    delete(channelId: number, issueId: number) {
        return this.#issueApiService.delete(channelId, issueId).pipe(
            switchMap(() => {
                return this.getIssues(channelId)
            })
        )
    }
}