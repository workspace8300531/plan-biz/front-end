import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { ICreateIssueCommentReq, ICreateIssueReq, IGetIssueDetailRes, IGetIssueRes } from "src/interface";
import { ProjectService } from "../project/project.service";

@Injectable({
    providedIn: 'root'
})
export class IssueApiService {
    readonly #projectService = inject(ProjectService)
    readonly #projectId = this.#projectService.projectId
    readonly #baseUrl = `${environment.server}/projects/${this.#projectId()}`
    readonly #http = inject(HttpClient);

    create(dto: ICreateIssueReq, channelId: number) {
        return this.#http.post<IGetIssueRes>(`${this.#baseUrl}/channels/${channelId}/issues`, dto);
    }

    createComment(dto: ICreateIssueCommentReq, channelId: number, issueId: number) {
        return this.#http.post<IGetIssueDetailRes>(`${this.#baseUrl}/channels/${channelId}/issues/${issueId}/comments`, dto);
    }

    toggleLike(channelId: number, issueId: number) {
        return this.#http.post<IGetIssueDetailRes>(`${this.#baseUrl}/channels/${channelId}/issues/${issueId}/likes`, {});
    }

    getIssues(channelId: number) {
        return this.#http.get<IGetIssueRes[]>(`${this.#baseUrl}/channels/${channelId}/issues`);
    }

    getIssue(channelId: number, issueId: number) {
        return this.#http.get<IGetIssueDetailRes>(`${this.#baseUrl}/channels/${channelId}/issues/${issueId}`);
    }

    searchIssues(keyword: string) {
        return this.#http.get<IGetIssueDetailRes[]>(`${this.#baseUrl}/channels/all/issues/search?keyword=${keyword}`);
    }

    update(dto: ICreateIssueReq, channelId: number, issueId: number) {
        return this.#http.put<IGetIssueRes>(`${this.#baseUrl}/channels/${channelId}/issues/${issueId}`, dto);
    }

    delete(channelId: number, issueId: number) {
        return this.#http.delete(`${this.#baseUrl}/channels/${channelId}/issues/${issueId}`);
    }
}