import { Injectable } from "@angular/core";
import { TokenService } from "./token/token.service";
import { TextZoom } from "@capacitor/text-zoom";
import { PlatformService } from "./platform.service";
import 'firebase/auth';
import { environment } from "src/environments/environment";

@Injectable({
    providedIn: 'root',
})
export class AppInitializerService {
    constructor(
        private readonly tokenService: TokenService,
        private readonly platformService: PlatformService,
    ) { }

    async initializeApp(): Promise<any> {
        if (!this.platformService.isWeb()) {
            await TextZoom.set({ value: 1.0 })
        }

        await this.tokenService.initToken();

        // const token = this.tokenService.token()
        // if (token.accessToken) {
        //     this.router.navigateByUrl('/main/home')
        // } 
        return true
    }
}

export function appInitializerFactory(appInitializerService: AppInitializerService): () => Promise<any> {
    return () => appInitializerService.initializeApp();
}
