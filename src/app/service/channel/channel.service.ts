import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { switchMap, tap } from "rxjs";
import { IGetChannelRes } from "src/interface/channel/channel.response";
import { ChannelApiService } from "./channel.api.service";

@Injectable({
    providedIn: 'root'
})
export class ChannelService {
    readonly #channelApiService = inject(ChannelApiService)

    readonly #channels: WritableSignal<IGetChannelRes[]> = signal([]);
    readonly channels = computed(() => this.#channels())

    //* 채널 아이디 상태로 관리
    readonly #channel: WritableSignal<IGetChannelRes | null> = signal(null);
    readonly channel = computed(() => this.#channel())

    //? 채널 생성
    create(formGroup: FormGroup, projectId: number) {
        const { name } = formGroup?.value as {
            name: string;
        }

        return this.#channelApiService.create({ name }, projectId).pipe(
            switchMap(() => {
                return this.getChannels(projectId)
            })
        )
    }

    //? 이름 존재하는지
    checkName(projectId: number, name: string) {
        return this.#channelApiService.checkName(projectId, name)
    }

    //? 채널 조회
    getChannels(projectId: number) {
        return this.#channelApiService.getChannels(projectId).pipe(
            tap((res) => {
                this.#channels.set(res)
            })
        )
    }

    //? 채널 검색 
    searchChannels(projectId: number, keyword: string) {
        return this.#channelApiService.search(projectId, keyword).pipe(
            tap((res) => {
                this.#channels.set(res)
            })
        )
    }

    //? 채널 수정
    update(formGroup: FormGroup, projectId: number, channelId: number) {
        const { name } = formGroup?.value as {
            name: string;
        }

        return this.#channelApiService.update({ name }, projectId, channelId).pipe(
            switchMap(() => {
                return this.getChannels(projectId)
            })
        )
    }

    //? 채널 삭제
    delete(projectId: number, channelId: number) {
        return this.#channelApiService.delete(projectId, channelId).pipe(
            switchMap(() => {
                return this.getChannels(projectId)
            })
        )
    }

    //? 채널 저장
    setChannel(channel: IGetChannelRes | null) {
        this.#channel.set(channel);
    }
}