import { HttpClient } from "@angular/common/http";
import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { environment } from "src/environments/environment";
import { ICreateChannelReq, ICreateLabelReq, IGetLabelDetailRes, IGetLabelRes } from "src/interface";
import { IGetChannelRes } from "src/interface/channel/channel.response";

@Injectable({
    providedIn: 'root'
})
export class ChannelApiService {
    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    create(dto: ICreateChannelReq, projectId: number) {
        return this.#http.post<IGetChannelRes>(`${this.#baseUrl}/projects/${projectId}/channels`, dto);
    }

    getChannels(projectId: number) {
        return this.#http.get<IGetChannelRes[]>(`${this.#baseUrl}/projects/${projectId}/channels`);
    }

    checkName(projectId: number, name: string) {
        return this.#http.get<IGetChannelRes[]>(`${this.#baseUrl}/projects/${projectId}/channels/check-name?name=${name}`);
    }

    search(projectId: number, keyword: string) {
        return this.#http.get<IGetChannelRes[]>(`${this.#baseUrl}/projects/${projectId}/channels/search?keyword=${keyword}`);
    }

    update(dto: ICreateChannelReq, projectId: number, channelId: number) {
        return this.#http.put<IGetChannelRes>(`${this.#baseUrl}/projects/${projectId}/channels/${channelId}`, dto);
    }

    delete(projectId: number, channelId: number) {
        return this.#http.delete(`${this.#baseUrl}/projects/${projectId}/channels/${channelId}`);
    }
}