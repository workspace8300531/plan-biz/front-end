import { HttpClient, HttpContext } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { SKIP_CREDENTIAL } from "src/app/core/interceptors/skip.token";
import { environment } from "src/environments/environment";
import { ICreateProjectReq, ICreateTokenReq, ICreateUserReq, IGetProfileRes, IGetProjectRes } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class TokenApiService {

    readonly #baseUrl = `${environment.server}/tokens`
    readonly #http = inject(HttpClient);

    sendEmail(dto: ICreateTokenReq) {
        return this.#http.post(`${this.#baseUrl}/password`, dto, { context: new HttpContext().set(SKIP_CREDENTIAL, true) });
    }
}