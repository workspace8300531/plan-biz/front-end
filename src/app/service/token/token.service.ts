import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { Preferences } from "@capacitor/preferences";
import { environment } from "src/environments/environment";
import { IGetTokenRes } from "src/interface";
import { TokenApiService } from "./token.api.service";
import { FormGroup } from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class TokenService {
    readonly #redirect = `${environment.deepLink}/update-password`;
    readonly #tokenApiService = inject(TokenApiService);
    readonly #apn = environment.apn

    readonly #token: WritableSignal<Pick<any, 'accessToken' | 'refreshToken'>> = signal({
        accessToken: '',
        refreshToken: ''
    });

    readonly token = computed(() => this.#token());

    //? 비밀번호 변경 토큰 전송
    sendEmail(formGroup: FormGroup) {
        const { email } = formGroup?.value as {
            email: string;
        };

        return this.#tokenApiService.sendEmail({
            email,
            redirect: this.#redirect,
            apn: this.#apn
        });
    }

    //? 토큰 초기 setting
    async initToken() {
        const token = await this.getToken();
        if (!token) {
            return
        }

        this.#token.set({ accessToken: token.accessToken, refreshToken: token.refreshToken });
    }

    //? 토큰 저장
    async setToken(tokens: IGetTokenRes) {
        this.#token.set({
            accessToken: tokens.accessToken,
            refreshToken: tokens.refreshToken
        })

        await Preferences.set({ key: 'accessToken', value: tokens.accessToken });
        await Preferences.set({ key: 'refreshToken', value: tokens.refreshToken });
    }

    //? 이메일 토큰 저장
    async setEmailToken(token: string) {
        await Preferences.set({ key: 'emailToken', value: token });
    }

    //? 토큰 가져옴
    async getToken() {
        const accessToken = await Preferences.get({ key: 'accessToken' });
        const refreshToken = await Preferences.get({ key: 'refreshToken' });

        return { accessToken: accessToken.value, refreshToken: refreshToken.value } as unknown as any
    }

    //? 이메일 토큰 가져옴
    async getEmailToken() {
        const token = await Preferences.get({ key: 'emailToken' });
        return token.value ?? undefined
    }

    //? 토큰 삭제
    async removeToken() {
        this.#token.set({
            accessToken: '',
            refreshToken: ''
        });

        Preferences.remove({ key: 'accessToken' });
        Preferences.remove({ key: 'refreshToken' });
    }

    async removeEmailToken() {
        Preferences.remove({ key: 'emailToken' });
    }

}