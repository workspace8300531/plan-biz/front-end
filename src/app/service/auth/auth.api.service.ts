import { HttpClient, HttpContext, HttpHeaders } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { SKIP_CREDENTIAL } from "src/app/core/interceptors/skip.token";
import { environment } from "src/environments/environment";
import { IGetTokenRes, ISignInReq } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class AuthApiService {

    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    //? 로그인
    signin(dto: ISignInReq, token?: string) {
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
        return this.#http.post<IGetTokenRes>(`${this.#baseUrl}/signin`, dto, { headers, context: new HttpContext().set(SKIP_CREDENTIAL, true) });
    }

    //? 로그아웃
    signOut() {
        return this.#http.delete(`${this.#baseUrl}/signout`);
    }

    //? 리프레쉬
    refresh(refreshToken: string) {
        const headers = { 'Authorization': `Bearer ${refreshToken}` };
        return this.#http.post<IGetTokenRes>(`${this.#baseUrl}/refresh`, {}, { headers, context: new HttpContext().set(SKIP_CREDENTIAL, true) });
    }
}