import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { catchError, tap, throwError } from "rxjs";
import { IGetProfileRes, IGetTokenRes } from "src/interface";
import { AuthApiService } from "./auth.api.service";
import { TokenService } from "../token/token.service";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    readonly #authApiService = inject(AuthApiService)
    readonly #tokenService = inject(TokenService)

    //? 로그인
    signin(formGroup: FormGroup, token?: string) {
        const { email, password } = formGroup?.value as {
            email: string;
            password: string;
        };

        return this.#authApiService.signin({
            email,
            password
        }, token).pipe(
            tap(async (tokens: IGetTokenRes) => {
                //* 토큰 서비스에서 저장하기
                this.#tokenService.setToken(tokens);
            })
        )
    }

    //? 로그아웃
    signout() {
        return this.#authApiService.signOut().pipe(
            tap(() => {
                this.#tokenService.removeToken();
            }),
            catchError((error) => {
                this.#tokenService.removeToken();
                return throwError(() => new Error('Sign out failed: ' + error));
            })
        );
    }

    //? 리프레쉬 토큰 발급
    refresh() {
        const { refreshToken } = this.#tokenService.token();
        console.log(refreshToken)
        return this.#authApiService.refresh(refreshToken).pipe(
            tap(async (tokens: IGetTokenRes) => {
                //* 토큰 서비스에서 저장하기
                this.#tokenService.setToken(tokens);
            })
        )
    }
}