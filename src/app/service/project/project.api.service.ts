import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { ICreateProjectReq, ICreateUserReq, IGetProfileRes, IGetProjectRes } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class ProjectApiService {

    readonly #baseUrl = `${environment.server}/projects`
    readonly #http = inject(HttpClient);

    create(dto: ICreateProjectReq) {
        return this.#http.post<IGetProjectRes>(`${this.#baseUrl}`, dto);
    }

    getProject(projectId: number) {
        return this.#http.get<IGetProjectRes>(`${this.#baseUrl}/${projectId}`);
    }

    getProjects() {
        return this.#http.get<IGetProjectRes[]>(`${this.#baseUrl}`);
    }

    update(dto: ICreateProjectReq, projectId: number) {
        return this.#http.put<IGetProjectRes>(`${this.#baseUrl}/${projectId}`, dto);
    }

    delete(projectId: number) {
        return this.#http.delete(`${this.#baseUrl}/${projectId}`);
    }
}