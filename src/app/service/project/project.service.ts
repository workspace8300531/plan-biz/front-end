import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { switchMap, tap } from "rxjs";
import { ICreateProjectReq, IGetProjectRes } from "src/interface";
import { ProjectApiService } from "./project.api.service";
import { FormGroup } from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class ProjectService {
    readonly #projectApiService = inject(ProjectApiService)

    readonly #projects: WritableSignal<IGetProjectRes[]> = signal([]);
    //* projects 필드는 #projects 현재 상태를 반환
    readonly projects = computed(() => this.#projects())

    //* 프로젝트 아이디 상태로 관리
    readonly #projectId: WritableSignal<number> = signal(0);
    readonly projectId = computed(() => this.#projectId())

    //? 프로젝트 생성
    create(formGroup: FormGroup) {
        const { name } = formGroup?.value as {
            name: string;
        };

        return this.#projectApiService.create({ name }).pipe(
            switchMap(() => {
                return this.getProjects()
            })
        )
    }

    //? 내가 참여한 프로젝트 조회
    getProjects() {
        return this.#projectApiService.getProjects().pipe(
            tap((res) => {
                this.#projects.set(res)
            })
        )
    }

    //? 프로젝트 상세 조회
    getProject(projectId: number) {
        return this.#projectApiService.getProject(projectId)
    }


    //? 프로젝트 삭제 (master일때만 가능!)
    update(dto: ICreateProjectReq, projectId: number) {
        return this.#projectApiService.update(dto, projectId).pipe(
            switchMap(() => {
                return this.getProjects()
            })
        )
    }

    delete(projectId: number) {
        return this.#projectApiService.delete(projectId).pipe(
            switchMap(() => {
                return this.getProjects()
            })
        )
    }

    //? 프로젝트 아이디 저장
    setProjectId(id: number) {
        this.#projectId.set(id);
    }
}