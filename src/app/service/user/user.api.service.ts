import { HttpClient, HttpContext, HttpHeaders } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { SKIP_CREDENTIAL } from "src/app/core/interceptors/skip.token";
import { environment } from "src/environments/environment";
import { ICreateProjectReq, ICreateUserReq, IGetProfileRes, IUpdatePasswordReq, IUpdateUserReq } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class UserApiService {

    readonly #baseUrl = `${environment.server}/users`
    readonly #http = inject(HttpClient);

    create(dto: ICreateUserReq, token?: string) {
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
        return this.#http.post<IGetProfileRes>(`${this.#baseUrl}`, dto, { headers, context: new HttpContext().set(SKIP_CREDENTIAL, true) });
    }

    getProfile() {
        return this.#http.get<IGetProfileRes>(`${this.#baseUrl}`);
    }

    updatePw(dto: IUpdatePasswordReq, token: string) {
        const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
        return this.#http.patch(`${this.#baseUrl}/password`, dto, { headers, context: new HttpContext().set(SKIP_CREDENTIAL, true) });
    }

    update(dto: IUpdateUserReq) {
        return this.#http.patch<IGetProfileRes>(`${this.#baseUrl}`, dto);
    }

    updateFormData(formData: FormData) {
        return this.#http.patch<IGetProfileRes>(`${this.#baseUrl}`, formData);
    }

    delete() {
        // return this.http.post<User.Response.Profile>(`${this.baseUrl}/api/v1/users`, signupForm);
    }
}