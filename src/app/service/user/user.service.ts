import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { from, switchMap, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { IGetProfileRes } from "src/interface";
import { PlatformService } from "../platform.service";
import { UserApiService } from "./user.api.service";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    readonly #platform = inject(PlatformService)
    readonly #userApiService = inject(UserApiService)
    readonly #redirect = `${environment.deepLink}/signin`
    readonly #apn = environment.apn
    readonly #user: WritableSignal<IGetProfileRes | null> = signal(null);
    readonly user = computed(() => this.#user())

    //? 회원정보
    profile() {
        return this.#userApiService.getProfile().pipe(
            tap((profile) => {
                this.#user.set(profile)
            })
        )
    }

    //? 회원가입
    signup(formGroup: FormGroup, token?: string) {
        const { username, password, email } = formGroup?.value as {
            username: string;
            password: string;
            email: string;
        };

        return this.#userApiService.create({
            username,
            password,
            email,
            redirect: this.#redirect,
            apn: this.#apn
        }, token).pipe(
            tap(async (tokens) => {
                //* 토큰 서비스에서 저장하기
            })
        )
    }

    //? 비밀번호 변경
    updatePw(formGroup: FormGroup) {
        const { password } = formGroup?.value as {
            password: string;
        };

        return this.#userApiService.update({
            password
        })
    }

    //? 비밀번호 토큰으로 변경
    updateForgotPw(formGroup: FormGroup, token: string) {
        const { password } = formGroup?.value as {
            password: string;
        };

        return this.#userApiService.updatePw({
            password
        }, token)
    }

    //? 회원정보 변경 
    update(formGroup: FormGroup) {
        const updateProcess = async () => {
            const formData = new FormData();
            const { username, email, thumbnail, type } = formGroup?.value;

            if (thumbnail) {
                const response = await fetch(thumbnail);
                const blob = await response.blob();
                const fileName = new Date().getTime() + `.${type}`;
                const file = new File([blob], fileName, { type: blob.type });
                formData.append('file', file);
            }

            formData.append('username', username);
            formData.append('email', email);

            //* 여기서 #userApiService.updateFormData는 Observable을 반환
            return this.#userApiService.updateFormData(formData).pipe(
                switchMap(() => {
                    return this.profile()
                })
            ).toPromise()
        };

        //* Promise를 Observable로 변환
        return from(updateProcess());
    }
}