import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { ICreateMilestoneReq, IGetMilestoneDetailRes, IGetMilestoneRes } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class MilestoneApiService {

    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    create(dto: ICreateMilestoneReq, projectId: number) {
        return this.#http.post<IGetMilestoneRes>(`${this.#baseUrl}/projects/${projectId}/milestones`, dto);
    }

    getMilestones(projectId: number) {
        return this.#http.get<IGetMilestoneRes[]>(`${this.#baseUrl}/projects/${projectId}/milestones`);
    }

    getMilestone(projectId: number, milestoneId: number) {
        return this.#http.get<IGetMilestoneDetailRes>(`${this.#baseUrl}/projects/${projectId}/milestones/${milestoneId}`);
    }

    searchMilestones(projectId: number, keyword: string) {
        return this.#http.get<IGetMilestoneRes[]>(`${this.#baseUrl}/projects/${projectId}/milestones/search?keyword=${keyword}`);
    }

    update(dto: ICreateMilestoneReq, projectId: number, milestoneId: number) {
        return this.#http.put<IGetMilestoneDetailRes>(`${this.#baseUrl}/projects/${projectId}/milestones/${milestoneId}`, dto);
    }

    delete(projectId: number, milestoneId: number) {
        return this.#http.delete<IGetMilestoneRes>(`${this.#baseUrl}/projects/${projectId}/milestones/${milestoneId}`);
    }
}