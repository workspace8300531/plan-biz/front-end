import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { switchMap, tap } from "rxjs";
import { ICreateMilestoneReq, IGetMilestoneRes, Priority } from "src/interface";
import { MilestoneApiService } from "./milestone.api.service";
import { FormGroup } from "@angular/forms";

@Injectable({
    providedIn: 'root'
})
export class MilestoneService {
    readonly #milestoneApiService = inject(MilestoneApiService)

    readonly #milestones: WritableSignal<IGetMilestoneRes[]> = signal([]);
    readonly milestones = computed(() => this.#milestones())

    //? 마일스톤 생성
    create(formGroup: FormGroup, projectId: number) {
        const { name, content, priority, labels } = formGroup?.value as {
            name: string;
            content: string;
            priority: Priority;
            labels: any
        };

        const labelIds = labels.map((res: any) => res.id);
        return this.#milestoneApiService.create({
            name,
            content,
            priority,
            labels: labelIds
        }, projectId).pipe(
            switchMap(() => {
                return this.getMilestones(projectId)
            })
        )
    }

    //? 마일스톤 조회
    getMilestones(projectId: number) {
        return this.#milestoneApiService.getMilestones(projectId).pipe(
            tap((res) => {
                this.#milestones.set(res)
            })
        )
    }

    //? 마일스톤 검색
    searchMilestones(projectId: number, keyword: string) {
        return this.#milestoneApiService.searchMilestones(projectId, keyword).pipe(
            tap((res) => {
                this.#milestones.set(res)
            })
        )
    }

    //? 마일스톤 상세조회
    getMilestone(projectId: number, milestoneId: number) {
        return this.#milestoneApiService.getMilestone(projectId, milestoneId)
    }

    //? 마일스톤 수정
    update(formGroup: FormGroup, projectId: number, milestoneId: number) {
        const { name, content, priority, labels } = formGroup?.value as {
            name: string;
            content: string;
            priority: Priority;
            labels: any
        };

        const labelIds = labels.map((res: any) => res.id);

        return this.#milestoneApiService.update({
            name,
            content,
            priority,
            labels: labelIds
        }, projectId, milestoneId).pipe(
            switchMap(() => {
                return this.getMilestones(projectId)
            })
        )
    }

    //? 마일스톤 삭제 
    delete(projectId: number, milestoneId: number) {
        return this.#milestoneApiService.delete(projectId, milestoneId).pipe(
            switchMap(() => {
                return this.getMilestones(projectId)
            })
        )
    }
}