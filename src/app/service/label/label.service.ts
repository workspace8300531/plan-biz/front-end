import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { switchMap, tap } from "rxjs";
import { IGetLabelRes } from "src/interface";
import { LabelApiService } from "./label.api.service";

@Injectable({
    providedIn: 'root'
})
export class LabelService {
    readonly #labelApiService = inject(LabelApiService)

    readonly #labels: WritableSignal<IGetLabelRes[]> = signal([]);
    readonly labels = computed(() => this.#labels())

    //? 라벨 생성
    create(formGroup: FormGroup, projectId: number) {
        const { name } = formGroup?.value as {
            name: string;
        }

        return this.#labelApiService.create({ name }, projectId).pipe(
            switchMap(() => {
                return this.getLabels(projectId)
            })
        )
    }

    //? 라벨 1회성 조회
    getLabelsOnce(projectId: number) {
        return this.#labelApiService.getLabels(projectId)
    }

    //? 라벨 조회
    getLabels(projectId: number) {
        return this.#labelApiService.getLabels(projectId).pipe(
            tap((res) => {
                this.#labels.set(res)
            })
        )
    }

    //? 라벨 상세 조회
    getLabel(projectId: number, labelId: number) {
        return this.#labelApiService.getLabel(projectId, labelId)
    }

    //? 라벨 검색
    searchLabels(projectId: number, keyword: string) {
        return this.#labelApiService.searchLabels(projectId, keyword).pipe(
            tap((res) => {
                this.#labels.set(res)
            })
        )
    }

    //? 라벨 이름 유효성 검사(이름이 존재하는지)
    checkName(projectId: number, name: string) {
        return this.#labelApiService.checkName(projectId, name)
    }

    //? 라벨 수정
    update(formGroup: FormGroup, projectId: number, labelId: number) {
        const { name } = formGroup?.value as {
            name: string;
        }

        return this.#labelApiService.update({ name }, projectId, labelId).pipe(
            switchMap(() => {
                return this.getLabels(projectId)
            })
        )
    }

    //? 라벨 삭제
    delete(projectId: number, labelId: number) {
        return this.#labelApiService.delete(projectId, labelId).pipe(
            switchMap(() => {
                return this.getLabels(projectId)
            })
        )
    }
}