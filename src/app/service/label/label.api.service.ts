import { HttpClient } from "@angular/common/http";
import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { environment } from "src/environments/environment";
import { ICreateLabelReq, IGetLabelDetailRes, IGetLabelRes } from "src/interface";

@Injectable({
    providedIn: 'root'
})
export class LabelApiService {
    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    create(dto: ICreateLabelReq, projectId: number) {
        return this.#http.post<IGetLabelRes>(`${this.#baseUrl}/projects/${projectId}/labels`, dto);
    }

    getLabels(projectId: number) {
        return this.#http.get<IGetLabelRes[]>(`${this.#baseUrl}/projects/${projectId}/labels`);
    }

    getLabel(projectId: number, labelId: number) {
        return this.#http.get<IGetLabelDetailRes>(`${this.#baseUrl}/projects/${projectId}/labels/${labelId}`);
    }

    searchLabels(projectId: number, keyword: string) {
        return this.#http.get<IGetLabelRes[]>(`${this.#baseUrl}/projects/${projectId}/labels/search?keyword=${keyword}`);
    }

    checkName(projectId: number, name: string) {
        return this.#http.get<IGetLabelRes[]>(`${this.#baseUrl}/projects/${projectId}/labels/check-name?name=${name}`);
    }

    update(dto: ICreateLabelReq, projectId: number, labelId: number) {
        return this.#http.put<IGetLabelDetailRes>(`${this.#baseUrl}/projects/${projectId}/labels/${labelId}`, dto);
    }

    delete(projectId: number, labelId: number) {
        return this.#http.delete(`${this.#baseUrl}/projects/${projectId}/labels/${labelId}`);
    }
}