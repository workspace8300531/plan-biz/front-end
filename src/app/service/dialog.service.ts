import { Injectable, inject } from "@angular/core";
import { ModalController, ToastController } from '@ionic/angular/standalone';
import { ModalOptions, ToastOptions } from '@ionic/core';

@Injectable({
    providedIn: 'root'
})
export class DialogService {

    readonly #modalCtrl = inject(ModalController);
    readonly #toastCtrl = inject(ToastController);

    async showModal(options: ModalOptions): Promise<HTMLIonModalElement> {
        const modal = await this.#modalCtrl.create(options);
        await modal.present();
        return modal;
    }
    async dismissModal(data?: any, id?: string): Promise<boolean> {
        return this.#modalCtrl.dismiss(data, undefined, id);
    }

    async dismissAllModals(): Promise<void> {
        const top = await this.#modalCtrl.getTop();
        if (top) {
            await this.#modalCtrl.dismiss();
            return this.dismissAllModals(); // 재귀적으로 모든 모달 닫기
        }
    }

    async showToast(options: ToastOptions): Promise<HTMLIonToastElement> {
        const toast = await this.#toastCtrl.create(options);
        await toast.present();
        return toast;
    }
}
