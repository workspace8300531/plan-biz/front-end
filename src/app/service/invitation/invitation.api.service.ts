import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { IGetInvitationRes } from "src/interface";
import { ICreateInvitationReq } from "src/interface/invitation/invitation.request";
import { IGetParticipantRes } from "src/interface/participant/participant.response";

@Injectable({
    providedIn: 'root'
})
export class InvitationApiService {

    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    create(dto: ICreateInvitationReq) {
        return this.#http.post(`${this.#baseUrl}/invitations`, dto);
    }

    getInvitation() {
        return this.#http.get<IGetInvitationRes[]>(`${this.#baseUrl}/invitations`);
    }

    updateInvitation(id: number) {
        return this.#http.patch<IGetParticipantRes[]>(`${this.#baseUrl}/invitations/${id}`, {});
    }
}