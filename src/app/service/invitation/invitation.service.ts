import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { InvitationApiService } from "./invitation.api.service";

@Injectable({
    providedIn: 'root'
})
export class InvitationService {
    readonly #invitationApiService = inject(InvitationApiService)
    readonly #redirect = `${environment.deepLink}/signin`
    readonly #apn = environment.apn

    //? 회원 초대
    create(projectId: number, email: string) {
        return this.#invitationApiService.create({ projectId, email, redirect: this.#redirect, apn: this.#apn })
    }

    //? 초대장 목록 조회
    getInvitation() {
        return this.#invitationApiService.getInvitation()
    }

    //? 회원 초대 수락
    updateInvitation(id: number) {
        return this.#invitationApiService.updateInvitation(id)
    }
}