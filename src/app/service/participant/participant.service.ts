import { Injectable, WritableSignal, computed, inject, signal } from "@angular/core";
import { IGetParticipantDetailsRes, IGetParticipantRes } from "src/interface/participant/participant.response";
import { ParticipantApiService } from "./participant.api.service";
import { switchMap, tap } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class ParticipantService {
    readonly #participantService = inject(ParticipantApiService)

    readonly #participants: WritableSignal<IGetParticipantRes[]> = signal([]);
    readonly participants = computed(() => this.#participants())

    readonly #participantDetails: WritableSignal<IGetParticipantDetailsRes[]> = signal([]);
    readonly participantDetails = computed(() => this.#participantDetails())

    //? 프로젝트 참여한 회원조회
    getParticipants(projectId: number) {
        return this.#participantService.getParticipants(projectId).pipe(
            tap((res) => {
                this.#participants.set(res)
                this.#participantDetails.set(res.map(p => ({
                    ...p,
                    username: p.user.username, // 최상위로 username 추출
                    email: p.user.email,
                    thumbnail: p.user.thumbnail       // 최상위로 email 추출
                })))
            })
        )
    }

    //? 프로젝트 참여한 회원검색
    searchParticipants(projectId: number, keyword: string) {
        return this.#participantService.searchParticipants(projectId, keyword).pipe(
            tap((res) => {
                this.#participants.set(res)
            })
        )
    }

    //? 프로젝트 추방
    delete(projectId: number, participantId: number) {
        return this.#participantService.delete(projectId, participantId).pipe(
            switchMap(() => {
                return this.getParticipants(projectId)
            })
        )
    }

    //? 프로젝트 나가기
    leave(projectId: number) {
        return this.#participantService.leave(projectId)
    }
}