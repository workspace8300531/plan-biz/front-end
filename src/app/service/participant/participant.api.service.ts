import { HttpClient } from "@angular/common/http";
import { Injectable, inject } from "@angular/core";
import { environment } from "src/environments/environment";
import { IGetParticipantRes } from "src/interface/participant/participant.response";

@Injectable({
    providedIn: 'root'
})
export class ParticipantApiService {

    readonly #baseUrl = `${environment.server}`
    readonly #http = inject(HttpClient);

    getParticipants(projectId: number) {
        return this.#http.get<IGetParticipantRes[]>(`${this.#baseUrl}/projects/${projectId}/participants`);
    }

    searchParticipants(projectId: number, keyword: string) {
        return this.#http.get<IGetParticipantRes[]>(`${this.#baseUrl}/projects/${projectId}/participants/search?keyword=${keyword}`);
    }

    leave(projectId: number) {
        return this.#http.delete(`${this.#baseUrl}/projects/${projectId}/participants`);
    }

    delete(projectId: number, participantId: number) {
        return this.#http.delete(`${this.#baseUrl}/projects/${projectId}/participants/${participantId}`);
    }
}