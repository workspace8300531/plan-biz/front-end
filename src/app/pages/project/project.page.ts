import { Component, inject } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterModule } from '@angular/router';
import { tap } from 'rxjs';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { fadeInOut } from 'src/app/core/animations/fade.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InvitationAcceptContainer } from 'src/app/core/containers/workspace/invitation-accept/invitation-accept.container';
import { ProjectRegisterContainer } from 'src/app/core/containers/workspace/project-register/project-register.container';
import { SideBarComponent } from 'src/app/core/layouts/side-bar/side-bar.component';
import { DialogService } from 'src/app/service/dialog.service';
import { InvitationService } from 'src/app/service/invitation/invitation.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { UserService } from 'src/app/service/user/user.service';


@Component({
  selector: 'app-project',
  templateUrl: './project.page.html',
  standalone: true,
  imports: [
    RouterModule,
    SideBarComponent,
    ButtonComponent
  ],
  animations: [fadeInOut]
})
export class ProjectPage {
  readonly #router = inject(Router);
  readonly #projectService = inject(ProjectService);
  readonly #userService = inject(UserService);
  readonly #invitationService = inject(InvitationService);
  readonly #dialogService = inject(DialogService);
  readonly projects = this.#projectService.projects;

  ngOnInit() {
    this.invitation();

    //* 프로필 불러오기  
    this.#userService.profile().subscribe()
  }

  //? 초대수락 안한 프로젝드 초대장 보여주기
  invitation() {
    this.#invitationService.getInvitation().subscribe((res) => {
      if (res.length === 0) {
        //* 모두 수락 완료시 프로젝트 페이지
        this.getProjects()

        return
      }

      this.openInvitation(res[0].project.id)
    })
  }

  //? 프로젝트 얻어오기
  getProjects() {
    this.#projectService.getProjects().pipe(
      tap((res) => {
        //* 가입한 프로젝트 없으면 - 프로젝트 만들기 페이지로 이동
        if (res.length === 0) {
          this.#router.navigateByUrl(`projects`)
          this.openProjectRegister(false);
        }
        //* 가입한 프로젝트 있으면 - 첫번째 프로젝트 마일스톤으로 이동
        else {
          this.#router.navigateByUrl(`projects/${res[0].id}/milestones`)
        }
      })
    ).subscribe();
  }

  //? 프로젝트 수락
  async openInvitation(projectId: number) {
    const modal = await this.#dialogService.showModal({
      component: InvitationAcceptContainer,
      cssClass: 'fullscreen-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        projectId
      }
    })

    modal.onWillDismiss().then((res) => {
      //* 가입한 프로젝트 얻어오기
      this.invitation()
    })
  }

  //? 프로젝트 등록
  async openProjectRegister(isProject: boolean) {
    const modal = await this.#dialogService.showModal({
      component: ProjectRegisterContainer,
      cssClass: 'fullscreen-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        isProject: isProject
      }
    })

    modal.onWillDismiss().then((res) => {
      //* 가입한 프로젝트 얻어오기
      this.getProjects()
    })
  }
}