import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { AbstractControl, COMPOSITION_BUFFER_MODE, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { IonAvatar } from "@ionic/angular/standalone";
import { of } from 'rxjs';
import { fadeInOut } from 'src/app/core/animations/fade.animation';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { ConfirmContainer } from 'src/app/core/containers/modal/confirm/confirm.container';
import { MemberRegisterContainer } from 'src/app/core/containers/workspace/member-register/member-register.container';
import { DialogService } from 'src/app/service/dialog.service';
import { ParticipantService } from 'src/app/service/participant/participant.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { Roles } from 'src/interface';

@Component({
  selector: 'app-member',
  templateUrl: './member.page.html',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonComponent,
    IonAvatar,
    MoreOptionButtonComponent
  ],
  animations: [fadeInOut],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class MemberPage {
  readonly #router = inject(Router);
  readonly #fb = inject(FormBuilder);
  readonly #participantService = inject(ParticipantService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly #projects = this.#projectService.projects;
  readonly #projectId = this.#projectService.projectId;
  readonly project = this.#projects().find((project) => { return project.id === Number(this.#projectId()) });
  readonly participants = this.#participantService.participants;
  readonly formGroup = this.#fb.group({
    name: ['', {
      asyncValidators: [this.memberValidator.bind(this)]
    }],
  })
  readonly Roles = Roles;

  ngOnInit() {
    //* 참여자 조회하기
    this.#participantService.getParticipants(this.#projectId()).subscribe();
  }

  private memberValidator(control: AbstractControl) {
    this.#participantService.searchParticipants(this.#projectId(), control.value).subscribe();

    return of(this.participants);
  }

  openRegister() {
    this.#dialogService.showModal({
      component: MemberRegisterContainer,
      cssClass: 'size-auto-modal'
    })
  }

  //? 회원추방
  memberDelete(participantId: number) {
    this.#participantService.delete(this.#projectId(), participantId).subscribe()
  }

  //? 프로젝트 나가기
  async leave() {
    const modal = await this.#dialogService.showModal({
      component: ConfirmContainer,
      cssClass: 'size-auto-modal',
      componentProps: {
        title: '프로젝트 탈퇴',
        subTitle: `<p>프로젝트에서 나가시겠습니까?<br>
          프로젝트 탈퇴 시, 초대를 받아야 재입장이 가능합니다</p>`,
        confirmText: '탈퇴하기',
      }
    })

    modal.onWillDismiss().then((res) => {
      if (res.data) {
        this.#participantService.leave(this.#projectId()).subscribe(() => {
          this.#router.navigate(['/'], { replaceUrl: true }).then(() => {
            //! 리로드 시켜줘야함 ... projects 페이지의 ngOnInit 실행시켜주기 위해서!!!!
            window.location.reload();
          });
        })
      }
    })
  }
}
