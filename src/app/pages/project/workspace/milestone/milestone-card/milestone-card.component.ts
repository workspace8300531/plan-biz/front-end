import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { IonAvatar } from "@ionic/angular/standalone";
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { IGetMilestoneRes } from 'src/interface';

import { } from '@ionic/angular';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { MilestoneRegisterContainer } from 'src/app/core/containers/workspace/milestone-register/milestone-register.container';
import { DialogService } from 'src/app/service/dialog.service';
import { ConfirmContainer } from 'src/app/core/containers/modal/confirm/confirm.container';
import { MilestoneService } from 'src/app/service/milestone/milestone.service';
import { ProjectService } from 'src/app/service/project/project.service';

@Component({
  selector: 'app-milestone-cards',
  templateUrl: './milestone-card.component.html',
  standalone: true,
  imports: [
    IonAvatar,
    CommonModule,
    LabelComponent,
    TagComponent,
    MoreOptionButtonComponent,
  ],
})
export class MilestoneCardComponent {
  @Input() milestone!: IGetMilestoneRes;
  readonly #dialogService = inject(DialogService);
  readonly #milestoneService = inject(MilestoneService);
  readonly #projectService = inject(ProjectService);
  readonly #projectId = this.#projectService.projectId;

  //? 마일스톤 수정
  async milestoneUpdate(name: string) {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: MilestoneRegisterContainer,
        data: {
          milestoneId: this.milestone.id,
          onDelete: () => this.milestoneDelete(name)
        }
      }
    })
  }

  //? 마일스톤 삭제
  async milestoneDelete(name: string) {
    const modal = await this.#dialogService.showModal({
      component: ConfirmContainer,
      cssClass: 'size-auto-modal',
      componentProps: {
        title: '칸반 삭제',
        subTitle: `<p><span class="text-bold text-neutral20">${name}</span> 마일스톤을 삭제하시겠습니까? 삭제 시 내용은 복구할 수 없습니다.</p>`,
        confirmText: '삭제',
        confirmColor: 'error',
      }
    })

    modal.onWillDismiss().then((res) => {
      if (res.data) {
        this.#milestoneService.delete(this.#projectId(), this.milestone.id).subscribe();
      }
    })
  }
}
