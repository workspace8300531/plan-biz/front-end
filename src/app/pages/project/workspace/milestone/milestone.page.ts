import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { switchMap } from 'rxjs';
import { fadeInOut } from 'src/app/core/animations/fade.animation';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { MilestoneService } from 'src/app/service/milestone/milestone.service';
import { MilestoneCardComponent } from './milestone-card/milestone-card.component';
import { ProjectService } from 'src/app/service/project/project.service';
import { DialogService } from 'src/app/service/dialog.service';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { MilestoneRegisterContainer } from 'src/app/core/containers/workspace/milestone-register/milestone-register.container';

@Component({
  selector: 'app-milestone',
  templateUrl: './milestone.page.html',
  standalone: true,
  imports: [
    RouterModule,
    ButtonComponent,
    MilestoneCardComponent
  ],
  animations: [fadeInOut]
})
export class MilestonePage {
  readonly #milestoneService = inject(MilestoneService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly milestones = this.#milestoneService.milestones;
  readonly projectId = this.#projectService.projectId;

  ngOnInit() {
    //* 마일스톤 조회
    this.#milestoneService.getMilestones(this.projectId()).subscribe();
  }

  //? 마일스톤 생성
  openRegister() {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: MilestoneRegisterContainer,
      }
    })
  }
}
