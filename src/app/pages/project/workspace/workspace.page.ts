import { Component, HostListener, effect, inject } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { switchMap, tap } from 'rxjs';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { TabsComponent } from 'src/app/core/components/tabs/tabs.component';
import { ProfileContainer } from 'src/app/core/containers/workspace/profile/profile.container';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { SideBarController } from 'src/app/core/layouts/side-bar/side-bar.controller';
import { DialogService } from 'src/app/service/dialog.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { UserService } from 'src/app/service/user/user.service';
import { IGetProjectRes } from 'src/interface';

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.page.html',
  standalone: true,
  imports: [
    IonicModule,
    RouterModule,
    TabsComponent,
    ProfileContainer,
  ]
})
export class WorkspacePage {
  readonly #router = inject(Router);
  readonly #route = inject(ActivatedRoute);
  readonly #projectService = inject(ProjectService);
  readonly #sideBarController = inject(SideBarController);
  readonly #dialogService = inject(DialogService);
  readonly #userService = inject(UserService)
  readonly #projectId = this.#projectService.projectId;
  readonly #projects = this.#projectService.projects;
  readonly user = this.#userService.user;
  project?: IGetProjectRes;
  isOpenProfile = false;

  constructor() {
    effect(() => {
      this.project = this.#projects().find((project) => { return project.id === Number(this.#projectId()) })
    })
  }

  ngOnInit() {
    this.getProject();
  }

  getProject() {
    this.#route.params.pipe(
      tap((params) => {
        //* 프로젝트 id 전역으로 관리 
        this.#projectService.setProjectId(params['projectId']);
        this.#router.navigate(['milestones'], { relativeTo: this.#route })
      }),
      tap(() => {
        this.project = this.#projects().find((project) => { return project.id === Number(this.#projectId()) })
      })
    ).subscribe();
  }

  onOpenNavigation() {
    this.#sideBarController.open();
  }

  //? 프로필 열기
  async openProfile() {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: ProfileContainer,
      }
    })
  }
}
