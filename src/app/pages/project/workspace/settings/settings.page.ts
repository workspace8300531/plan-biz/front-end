import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { validatePassword } from 'firebase/auth';
import { fadeInOut } from 'src/app/core/animations/fade.animation';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { ConfirmContainer } from 'src/app/core/containers/modal/confirm/confirm.container';
import { ValidationConfirmContainer } from 'src/app/core/containers/modal/validation-confirm/validation-confirm.container';
import { DialogService } from 'src/app/service/dialog.service';
import { ProjectService } from 'src/app/service/project/project.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ButtonComponent,
    InputLabelComponent
  ],
  animations: [fadeInOut]
})
export class SettingsPage {
  readonly #fb = inject(FormBuilder);
  readonly #router = inject(Router)
  readonly #projectService = inject(ProjectService);
  readonly #projectId = this.#projectService.projectId;
  readonly #dialogService = inject(DialogService);
  readonly #projects = this.#projectService.projects;
  readonly formGroup = this.#fb.group({
    name: ['', {
      validators: [Validators.required]
    }],
  })

  //? 프로젝트 수정
  async projectUpdate() {
    if (!this.formGroup.value.name) {
      return
    }

    this.#projectService.update({ name: this.formGroup.value.name }, this.#projectId()).subscribe();
  }

  //? 프로젝트 삭제
  async projectDelete() {
    const project = this.#projects().find((project) => { return project.id === Number(this.#projectId()) })

    const modal = await this.#dialogService.showModal({
      component: ValidationConfirmContainer,
      cssClass: 'size-auto-modal',
      componentProps: {
        title: '프로젝트 삭제',
        subTitle: `<p>삭제할 프로젝트의 이름 <span class="text-bold text-neutral20">${project?.name}</span>을(를) 입력해주세요. 프로젝트 삭제시 프로젝트는 즉시 삭제되며, 복구할 수 없습니다.</p>`,
        confirmText: '삭제',
        confirmColor: 'error',
        validationMessage: `${project?.name}`
      }
    })

    modal.onWillDismiss().then((res) => {
      if (res.data) {
        this.#projectService.delete(this.#projectId()).subscribe();
        this.#router.navigate(['/projects'], { replaceUrl: true }).then(() => {
          //! 리로드 시켜줘야함 ... projects 페이지의 ngOnInit 실행시켜주기 위해서!!!!
          window.location.reload();
        });
      }
    })
  }
}
