import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { IonAvatar } from "@ionic/angular/standalone";
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { StatusTagComponent } from 'src/app/core/components/status-tag/status-tag.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { KanbanRegisterContainer } from 'src/app/core/containers/workspace/kanban-register/kanban-register.container';
import { DialogService } from 'src/app/service/dialog.service';
import { KanbanService } from 'src/app/service/kanban/kanban.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetKanbanRes, Status } from 'src/interface';
import { ConfirmContainer } from 'src/app/core/containers/modal/confirm/confirm.container';

@Component({
  selector: 'app-kanban-card',
  templateUrl: './kanban-card.component.html',
  standalone: true,
  imports: [
    IonAvatar,
    CommonModule,
    LabelComponent,
    TagComponent,
    MoreOptionButtonComponent,
    StatusTagComponent
  ]
})
export class KanbanCardComponent {
  @Input() milestoneType = false;
  @Input() kanban!: IGetKanbanRes;
  readonly #kanbanService = inject(KanbanService)
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly #projectId = this.#projectService.projectId;
  readonly status = Status;

  //? 칸반 수정
  async kanbanUpdate(name: string) {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: KanbanRegisterContainer,
        data: {
          kanbanId: this.kanban.id,
          onDelete: () => this.kanbanDelete(name) //! 참조로 전달해야함
        }
      }
    })
  }

  //? 칸반 삭제
  async kanbanDelete(name: string) {
    const modal = await this.#dialogService.showModal({
      component: ConfirmContainer,
      cssClass: 'size-auto-modal',
      componentProps: {
        title: '칸반 삭제',
        subTitle: `<p><span class="text-bold text-neutral20">${name}</span> 칸반을 삭제하시겠습니까? 삭제 시 내용은 복구할 수 없습니다.</p>`,
        confirmText: '삭제',
        confirmColor: 'error',
      }
    })

    modal.onWillDismiss().then((res) => {
      if (res.data) {
        this.#kanbanService.delete(this.#projectId(), this.kanban.id).subscribe();
      }
    })
  }
}