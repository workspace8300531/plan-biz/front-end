import {
  CdkDrag,
  CdkDragDrop,
  CdkDropList,
  moveItemInArray
} from '@angular/cdk/drag-drop';
import { CommonModule } from "@angular/common";
import { Component, inject } from "@angular/core";
import { KanbanService } from 'src/app/service/kanban/kanban.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { Status } from 'src/interface';
import { KanbanCardComponent } from "./kanban-card/kanban-card.component";
import { DialogService } from 'src/app/service/dialog.service';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { KanbanRegisterContainer } from 'src/app/core/containers/workspace/kanban-register/kanban-register.container';
@Component({
  selector: 'app-kanban',
  templateUrl: './kanban.page.html',
  standalone: true,
  imports: [
    CommonModule,
    KanbanCardComponent,
    CdkDropList,
    CdkDrag
  ],
})
export class KanbanPage {
  readonly status = Status;
  readonly #kanbanService = inject(KanbanService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly projectId = this.#projectService.projectId;
  readonly kanbanWaitings = this.#kanbanService.kanbanWaitings;
  readonly kanbanProgresses = this.#kanbanService.kanbanProgress;
  readonly kanbanReviews = this.#kanbanService.kanbanReview;
  readonly kanbanCompletes = this.#kanbanService.kanbanCompleted;

  ngOnInit() {
    //* 칸반 조회하기
    this.#kanbanService.getKanbans(this.projectId()).subscribe();
  }

  //? 칸반 생성 
  async openRegister(status: Status) {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: KanbanRegisterContainer,
        data: {
          statusValue: status
        }
      }
    })
  }

  drop(event: CdkDragDrop<any>, status: Status) {
    if (event.previousContainer === event.container) {
      const previousIndex = event.previousIndex;
      const currentIndex = event.currentIndex;
      const kanbans = event.previousContainer.data
      const previousKanban = kanbans[previousIndex];
      const currentKanban = kanbans[currentIndex];

      this.#kanbanService.updateKanbanStatus(this.projectId(), {
        previousKanban: previousKanban,
        currentKanban: currentKanban,
        previousStatus: status,
        currentStatus: status,
        previousIndex: previousIndex,
        currentIndex: currentIndex
      }).subscribe();

    } else {
      const previousIndex = event.previousIndex;
      const currentIndex = event.currentIndex;
      const kanbans = event.previousContainer.data

      const previousKanban = kanbans[previousIndex];
      this.#kanbanService.updateKanbanStatus(this.projectId(), {
        previousKanban: previousKanban,
        currentStatus: status,
        currentIndex: currentIndex,
        previousIndex: previousIndex
      }).subscribe();
    }
  }
}