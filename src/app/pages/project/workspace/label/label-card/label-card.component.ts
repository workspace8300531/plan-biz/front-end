import { Component, Input, inject } from '@angular/core';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { LabelRegisterContainer } from 'src/app/core/containers/workspace/label-register/label-register.container';
import { DialogService } from 'src/app/service/dialog.service';
import { LabelService } from 'src/app/service/label/label.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetLabelRes } from 'src/interface';

@Component({
  selector: 'app-label-card',
  templateUrl: './label-card.component.html',
  standalone: true,
  imports: [
    LabelComponent,
    MoreOptionButtonComponent
  ],
})
export class LabelCardComponent {
  @Input() label!: IGetLabelRes
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly #labelService = inject(LabelService);
  readonly #projectId = this.#projectService.projectId;

  //? 라벨 수정
  async labelUpdate() {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: LabelRegisterContainer,
        data: { labelId: this.label.id }
      }
    })
  }

  //? 라벨 삭제
  async labelDelete() {
    this.#labelService.delete(this.#projectId(), this.label.id).subscribe();
  }
}