import { CommonModule } from '@angular/common';
import { Component, OnInit, WritableSignal, effect, inject, signal } from '@angular/core';
import { AbstractControl, COMPOSITION_BUFFER_MODE, FormBuilder, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { LabelService } from 'src/app/service/label/label.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { LabelCardComponent } from './label-card/label-card.component';
import { of, tap } from 'rxjs';
import { DialogService } from 'src/app/service/dialog.service';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { LabelRegisterContainer } from 'src/app/core/containers/workspace/label-register/label-register.container';
import { IGetLabelRes } from 'src/interface';
import { LabelDetailContainer } from 'src/app/core/containers/workspace/label-detail/label-detail.container';

@Component({
  selector: 'app-label',
  templateUrl: './label.page.html',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ButtonComponent,
    LabelCardComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class LabelPage {
  readonly #labelService = inject(LabelService);
  readonly #fb = inject(FormBuilder);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly #projectId = this.#projectService.projectId;
  readonly labels = this.#labelService.labels;

  readonly formGroup = this.#fb.group({
    name: ['', {
      asyncValidators: [this.labelValidator.bind(this)]
    }],
  })

  //? 라벨 생성
  async openRegister() {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: LabelRegisterContainer,
      }
    })
  }


  //? 라벨 상세보기
  async openLabelDetail(labelId: number) {
    this.#dialogService.showModal({
      component: LabelDetailContainer,
      cssClass: 'fullscreen-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        labelId
      }
    })
  }

  //? 비동기 validators을 통해 값이 변경될때마다 라벨 검색하기
  private labelValidator(control: AbstractControl) {
    this.#labelService.searchLabels(this.#projectId(), control.value).subscribe();

    return of(this.labels);
  }
}
