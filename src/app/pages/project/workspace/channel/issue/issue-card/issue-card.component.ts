import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { IonAvatar } from "@ionic/angular/standalone";
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { IssueRegisterContainer } from 'src/app/core/containers/workspace/issue-register/issue-register.container';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { DialogService } from 'src/app/service/dialog.service';
import { IssueService } from 'src/app/service/issue/issue.service';
import { IGetIssueRes } from 'src/interface';

@Component({
  selector: 'app-issue-card',
  templateUrl: './issue-card.component.html',
  standalone: true,
  imports: [
    IonAvatar,
    CommonModule,
    LabelComponent,
    MoreOptionButtonComponent,
    TagComponent
  ],
})
export class IssueCardComponent {
  @Input() issue!: IGetIssueRes
  readonly #dialogService = inject(DialogService);
  readonly #issueService = inject(IssueService);
  readonly #channelService = inject(ChannelService);
  readonly #channel = this.#channelService.channel

  // ? 이슈 수정
  async issueUpdate() {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: IssueRegisterContainer,
        data: { issueId: this.issue.id }
      }
    })
  }

  //? 이슈 삭제
  async issueDelete() {
    this.#issueService.delete(this.#channel()?.id!, this.issue.id).subscribe();
  }
}
