import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { of, switchMap } from 'rxjs';
import { EnterAnimation } from 'src/app/core/animations/enter.animation';
import { LeaveAnimation } from 'src/app/core/animations/leave.animation';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { IssueDetailContainer } from 'src/app/core/containers/workspace/issue-detail/issue-detail.container';
import { IssueRegisterContainer } from 'src/app/core/containers/workspace/issue-register/issue-register.container';
import { RightSideBarComponent } from 'src/app/core/layouts/right-side-modal/right-side-bar.component';
import { SideBarComponent } from 'src/app/core/layouts/side-bar/side-bar.component';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { DialogService } from 'src/app/service/dialog.service';
import { IssueService } from 'src/app/service/issue/issue.service';
import { IssueCardComponent } from './issue-card/issue-card.component';
import { IssueCommentCardComponent } from '../../../../../core/containers/workspace/issue-detail/issue-comment-card/issue-comment-card.component';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.page.html',
  standalone: true,
  imports: [
    RouterModule,
    SideBarComponent,
    ButtonComponent,
    IssueCardComponent,
    IssueCommentCardComponent
  ],
})
export class IssuePage implements OnInit {
  readonly #route = inject(ActivatedRoute);
  readonly #channelService = inject(ChannelService);
  readonly #issueService = inject(IssueService);
  readonly #dialogService = inject(DialogService);
  readonly issues = this.#issueService.issues;
  readonly channels = this.#channelService.channels;

  ngOnInit() {
    this.#route.params.pipe(
      switchMap((params) => {
        const channelId = params['channelId']

        //* 상태로 관리
        this.#channelService.setChannel(this.channels().find((channel) => { return channel.id === Number(channelId) })!)
        return of(channelId)
      }),
      switchMap((res) => {
        //* 이슈 조회
        return this.#issueService.getIssues(res)
      })
    ).subscribe();
  }

  //? 이슈 생성
  openRegister() {
    this.#dialogService.showModal({
      component: RightSideBarComponent,
      cssClass: 'custom-right-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        container: IssueRegisterContainer,
      }
    })
  }

  //? 이슈 상세보기
  async openIssueDetail(issueId: number) {
    this.#dialogService.showModal({
      component: IssueDetailContainer,
      cssClass: 'fullscreen-modal',
      enterAnimation: EnterAnimation,
      leaveAnimation: LeaveAnimation,
      componentProps: {
        issueId
      }
    })
  }
}
