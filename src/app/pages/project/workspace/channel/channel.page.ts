import { Component, OnInit, effect, inject } from '@angular/core';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { fadeInOut } from 'src/app/core/animations/fade.animation';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { SideBarComponent } from 'src/app/core/layouts/side-bar/side-bar.component';
import { ChannelRegisterContainer } from 'src/app/core/containers/workspace/channel-register/channel-register.container';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { DialogService } from 'src/app/service/dialog.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { AbstractControl, FormBuilder } from '@angular/forms';
import { ActionSheetContainer } from 'src/app/core/containers/modal/action-sheet/action-sheet.container';

@Component({
  selector: 'app-channel',
  templateUrl: './channel.page.html',
  standalone: true,
  imports: [
    RouterModule,
    RouterModule,
    SideBarComponent,
    ButtonComponent,
    MoreOptionButtonComponent
  ],
  animations: [fadeInOut]
})
export class ChannelPage implements OnInit {
  readonly #router = inject(Router);
  readonly #route = inject(ActivatedRoute);
  readonly #channelService = inject(ChannelService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly channels = this.#channelService.channels;
  readonly #projectId = this.#projectService.projectId;
  readonly channel = this.#channelService.channel;
  readonly #fb = inject(FormBuilder);
  readonly formGroup = this.#fb.group({
    name: [''],
  })
  readonly channelFormGroup = this.#fb.group({
    name: ['', {
      asyncValidators: [this.channelValidator.bind(this)]
    }],
  })

  constructor() {
    effect(() => {
      console.log(this.channels())
    })
  }

  ngOnInit() {
    //* 채널 검색
    this.formGroup.get('name')?.valueChanges.pipe(
      switchMap((res) => {
        return this.#channelService.searchChannels(this.#projectId(), res ?? "")
      })
    ).subscribe()

    //* 채널 조회
    this.#channelService.getChannels(this.#projectId()).pipe(
      tap(() => {
        this.managerChannel();
      })
    ).subscribe()
  }

  //? 채널 생성
  openRegister() {
    this.#dialogService.showModal({
      component: ChannelRegisterContainer,
      cssClass: 'size-auto-modal'
    })
  }

  //? 채널 수정
  channelUpdate(channelId: number) {
    this.#dialogService.showModal({
      component: ChannelRegisterContainer,
      cssClass: 'size-auto-modal',
      componentProps: {
        channelId
      }
    })
  }

  channelDelete(channelId: number) {
    this.#channelService.delete(this.#projectId(), channelId).subscribe();
  }

  //? 채널 변경(모바일)
  async openChangeChannel() {

    const modal = await this.#dialogService.showModal({
      component: ActionSheetContainer,
      cssClass: 'custom-bottom-modal',
      componentProps: {
        name: "채널",
        list: this.channels(),
        formGroup: this.formGroup
      }
    });

    modal.onWillDismiss().then((res) => {
      if (!res.data) {
        return
      }

      //* 채널 생성 버튼 열기
      if (res.data === 'submit') {
        this.openRegisterChannel()
      }
      //* 채널 조회 
      else {
        this.#router.navigateByUrl(`/projects/${this.#projectId()}/channels/${res.data.id}/issues`);
      }
    })
  }

  //? 채널 생성(모바일)
  async openRegisterChannel() {
    const modal = await this.#dialogService.showModal({
      component: ActionSheetContainer,
      cssClass: 'custom-bottom-modal',
      componentProps: {
        name: "채널",
        formGroup: this.channelFormGroup
      }
    });

    modal.onWillDismiss().then((res) => {
      if (res.data === 'submit') {
        this.#channelService.create(this.channelFormGroup, this.#projectId()).subscribe();
      }
    })
  }

  //? 비동기 Validators를 통해 라벨이름이 존재하는지 유효성 체크
  private channelValidator(control: AbstractControl) {
    return this.#channelService.checkName(this.#projectId(), control.value).pipe(
      map(() => {
        return null;
      }),
      catchError(() => of({ invalidChannelName: true }))
    );
  }

  private managerChannel() {
    const channels = this.channels();

    //* 채널이 하나 이상 있는지 확인 
    const hasChannels = channels.length !== 0;

    if (hasChannels) {
      //* 채널이 있으면 첫 번째 채널을 사용
      const firstChannel = channels[0];

      //* 라우터를 사용하여 첫 번째 채널의 'issues' 페이지로 이동 
      // *'relativeTo' 옵션은 현재 라우트를 기준으로 상대 경로를 설정
      this.#router.navigate([firstChannel.id, 'issues'], { relativeTo: this.#route });
    } else {
      //* 채널이 없으면 채널 상태를 null로 설정
      this.#channelService.setChannel(null);
    }
  }
}
