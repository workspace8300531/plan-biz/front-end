import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { FirebaseDynamicLinks } from '@pantrist/capacitor-firebase-dynamic-links';
import { tap } from 'rxjs';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { PASSWORD_PATTERN } from 'src/app/core/utils/patterns';
import { TokenService } from 'src/app/service/token/token.service';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.page.html',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ButtonComponent,
    InputLabelComponent
  ]
})
export class UpdatePasswordPage {
  readonly #fb = inject(FormBuilder);
  readonly #router = inject(Router);
  readonly #route = inject(ActivatedRoute);
  readonly #tokenService = inject(TokenService);
  readonly #userService = inject(UserService);
  readonly formGroup: FormGroup = this.#fb.group({
    password: new FormControl('', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]),
    rePassword: new FormControl('', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]),
  });

  ngOnInit() {
    console.log('ngOnInit')
    //* 이메일 토큰 저장
    this.#route.queryParams.pipe(
      tap((params) => {
        if (!params['token']) {
          return
        }

        this.#tokenService.setEmailToken(params['token'])
      }),
    ).subscribe()
  }

  //? 비밀번호 변경
  async onSubmit() {
    const token = await this.#tokenService.getEmailToken()

    if (!token) {
      return
    }

    this.#userService.updateForgotPw(this.formGroup, token).subscribe({
      next: async () => {
        this.#tokenService.removeEmailToken();
        //* 변경 성공시 로그인페이지로 이동
        this.navSignIn()
      },
    });
  }

  navSignIn(): void {
    this.#router.navigate(['/signin'], { replaceUrl: true });
  }
}