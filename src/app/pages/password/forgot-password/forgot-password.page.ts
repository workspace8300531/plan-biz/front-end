import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { EMAIL_PATTERN } from 'src/app/core/utils/patterns';
import { TokenService } from 'src/app/service/token/token.service';
import { tap } from 'rxjs';
import { MessageContainer } from 'src/app/core/containers/modal/message/message.container';
import { DialogService } from 'src/app/service/dialog.service';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  standalone: true,
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ButtonComponent,
    InputLabelComponent
  ]
})
export class ForgotPasswordPage {
  readonly #fb = inject(FormBuilder);
  readonly #router = inject(Router);
  readonly #tokenService = inject(TokenService);
  readonly #dialogService = inject(DialogService);

  formGroup: FormGroup = this.#fb.group({
    email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)])
  })

  navSignIn(): void {
    this.#router.navigate(['/signin'], { replaceUrl: true });
  }

  onSubmit() {
    this.#tokenService.sendEmail(this.formGroup).pipe(
      tap(async () => {
        const modal = await this.#dialogService.showModal({
          component: MessageContainer,
          cssClass: 'size-auto-modal',
          componentProps: {
            title: '이메일 전송 완료',
            message: '이메일을 확인해주세요.'
          }
        })

        modal.onWillDismiss().then(() => {
          this.navSignIn()
        })
      })
    ).subscribe()
  }
}