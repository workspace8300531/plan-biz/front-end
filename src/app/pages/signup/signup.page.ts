import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterModule } from '@angular/router';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { MessageContainer } from 'src/app/core/containers/modal/message/message.container';
import { EMAIL_PATTERN, PASSWORD_PATTERN } from 'src/app/core/utils/patterns';
import { DialogService } from 'src/app/service/dialog.service';
import { UserService } from 'src/app/service/user/user.service';
import { IonContent } from "@ionic/angular/standalone";
import { TokenService } from 'src/app/service/token/token.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  standalone: true,
  imports: [
    IonContent,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ButtonComponent,
    InputLabelComponent
  ]
})
export class SignupPage {
  readonly #fb = inject(FormBuilder);
  readonly #router = inject(Router);
  readonly #userService = inject(UserService);
  readonly #dialogService = inject(DialogService);
  readonly #tokenService = inject(TokenService);

  formGroup: FormGroup = this.#fb.group({
    username: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(12)]),
    email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
    password: new FormControl('', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]),
    isAgreed: new FormControl(''),
    phone: new FormControl(''),
    rePassword: new FormControl('', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]),
  });

  //? 회원가입
  async onSignUp() {
    const token = await this.#tokenService.getEmailToken()

    this.#userService.signup(this.formGroup, token).subscribe({
      next: async () => {
        //* 성공시 로그인페이지로 이동시키기
        this.#router.navigateByUrl('/signin')
      },
      error: async err => {
        //* 실패시 이유 띄워주기
        await this.#dialogService.showModal({
          component: MessageContainer,
          cssClass: 'size-auto-modal',
          componentProps: {
            title: '회원가입 실패',
            message: err.error.message
          }
        })
      }
    });
  }
} 
