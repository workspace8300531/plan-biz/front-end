import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { App } from '@capacitor/app';
import { IonContent } from "@ionic/angular/standalone";
import { tap } from 'rxjs';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { MessageContainer } from 'src/app/core/containers/modal/message/message.container';
import { AuthService } from 'src/app/service/auth/auth.service';
import { DialogService } from 'src/app/service/dialog.service';
import { TokenService } from 'src/app/service/token/token.service';
import {
  FirebaseDynamicLinks,
  LinkConfig,
} from '@pantrist/capacitor-firebase-dynamic-links';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  standalone: true,
  imports: [
    IonContent,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    ButtonComponent,
    InputLabelComponent
  ]
})
export class SigninPage {
  readonly #fb = inject(FormBuilder);
  readonly #router = inject(Router);
  readonly #route = inject(ActivatedRoute);
  readonly #authService = inject(AuthService);
  readonly #dialogService = inject(DialogService);
  readonly #tokenService = inject(TokenService);
  formGroup: FormGroup = this.#fb.group({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  async ngOnInit() {
    //* web deep link
    this.#route.queryParams.pipe(
      tap((params) => {
        if (!params['token']) {
          return
        }
        this.#tokenService.setEmailToken(params['token'])
      }),
    ).subscribe();

    //* apn deep
    FirebaseDynamicLinks.addListener('deepLinkOpen', async (data) => {
      const url = new URL(data.url);
      const token = url.searchParams.get('token');

      if (!token) {
        return
      }

      await this.#tokenService.setEmailToken(token);
      const baseUrl = "https://planbiz.hohoco.store";
      const relativePath = url.href.substring(baseUrl.length);
      this.#router.navigateByUrl(relativePath)
    });
  }

  //? 로그인
  async onSignIn() {
    const token = await this.#tokenService.getEmailToken()

    this.#authService.signin(this.formGroup, token).subscribe({
      next: async () => {
        this.#tokenService.removeEmailToken();
        //* 로그인 성공시 projects 페이지로 이동
        this.#router.navigateByUrl('/projects')
      },
      error: async err => {
        //* 실패시 이유 띄워주기
        await this.#dialogService.showModal({
          component: MessageContainer,
          cssClass: 'size-auto-modal',
          componentProps: {
            title: '로그인 실패',
            message: err.error.message
          }
        })
      }
    });
  }
}
