import { Component } from '@angular/core';
import { IonHeader, IonToolbar, IonTitle, IonContent } from '@ionic/angular/standalone';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { InputComponent } from 'src/app/core/components/input/input.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  standalone: true,
  imports: [
    InputComponent,
    InputLabelComponent,
    ButtonComponent,
    TagComponent
  ],
})
export class HomePage {
  constructor() { }
}
