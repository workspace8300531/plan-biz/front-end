import { Injectable, WritableSignal, computed, signal } from "@angular/core";

import { BehaviorSubject } from "rxjs";
import { NavParams } from "./side-bar.type";

@Injectable({
    providedIn: 'root'
})
export class SideBarController {

    readonly #sideBarParams: WritableSignal<NavParams> = signal({
        width: 256,
        position: 'static',
        visible: true
    });
    readonly sideBarParams = computed(() => this.#sideBarParams())

    //? 왼쪽 사이드바 열기
    open() {
        this.#sideBarParams.set({
            ...this.#sideBarParams(),
            visible: true
        })
    }

    //? 왼쪽 사이드바 닫기
    close() {
        this.#sideBarParams.set({
            ...this.#sideBarParams(),
            visible: false
        })
    }

    //? 화면이 데스크탑에서 모바일로 변환 될 때
    //? 모바일 화면으로 리프레시할 때 사용
    changeFixedPosition() {
        this.#sideBarParams.set({
            ...this.#sideBarParams(),
            visible: false,
            position: 'fixed'
        });
    }

    //? 화면이 모바일에서 데스크탑으로 변환 될 때
    //? 데스크탑 화면으로 리프레시할 때 사용 
    changeStaticPosition() {
        this.#sideBarParams.set({
            ...this.#sideBarParams(),
            visible: true,
            position: 'static'
        });
    }
}