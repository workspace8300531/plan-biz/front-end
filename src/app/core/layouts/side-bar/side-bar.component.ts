import { CommonModule } from '@angular/common';
import { Component, DestroyRef, OnInit, inject } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

import { fromEvent, tap } from 'rxjs';

import { fadeInOut } from 'src/app/core/animations/fade.animation';
import { SideBarController } from './side-bar.controller';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
  standalone: true,
  imports: [CommonModule],
  animations: [fadeInOut],
})
export class SideBarComponent implements OnInit {
  private readonly DESKTOP_MIN_WIDTH = 1024;
  readonly #sidebarController = inject(SideBarController);
  readonly #destroyRef = inject(DestroyRef)
  readonly sideBarParams = this.#sidebarController.sideBarParams;

  ngOnInit(): void {
    this.updateNavParams();

    fromEvent(window, 'resize')
      .pipe(
        tap(() => this.updateNavParams()),
        takeUntilDestroyed(this.#destroyRef)
      )
      .subscribe();
  }

  onCloseNavigation() {
    this.#sidebarController.close();
  }

  private updateNavParams() {
    if (window.innerWidth >= this.DESKTOP_MIN_WIDTH) {
      this.#sidebarController.changeStaticPosition();
      return;
    }

    this.#sidebarController.changeFixedPosition();
  }
}
