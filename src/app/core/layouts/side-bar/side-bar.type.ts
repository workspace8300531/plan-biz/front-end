export interface NavParams {
    width: number;
    position: 'static' | 'fixed';
    visible: boolean;
}