import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { fadeInOut } from '../../animations/fade.animation';
import { slideIn } from '../../animations/slide-in.animation';
import { DynamicComponentDirective } from '../../directives/dynamic-component.directive';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';

@Component({
  selector: 'app-right-side-bar',
  templateUrl: './right-side-bar.component.html',
  standalone: true,
  imports: [
    CommonModule,
    IonicModule,
    DynamicComponentDirective,
    ToolbarComponent
  ],
})
export class RightSideBarComponent {
  @Input() container: any;
  @Input() data: any;
}
