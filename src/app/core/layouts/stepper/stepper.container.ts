import { CdkStepper } from '@angular/cdk/stepper';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';


@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.container.html',
  standalone: true,
  imports: [
    CommonModule
  ],
  providers: [
    {
      provide: CdkStepper,
      useExisting: StepperContainer
    }
  ]
})
export class StepperContainer extends CdkStepper { }
