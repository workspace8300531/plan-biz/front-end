import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { LabelComponent } from '../label/label.component';
import { ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-select-box',
  templateUrl: './select-box.component.html',
  standalone: true,
  imports: [
    LabelComponent,
  ],
})
export class SelectBoxComponent {
  @Input() name = "태그"
  @Input() list: { id: number, name: string }[] = []
  @Input() selectedItem?: { id: number, name: string }
  @Output() selectedEm = new EventEmitter<{ id: number, name: string }>();
  openDropdown = false;

  //? 선택 및 선택된 값 방출
  onSelected(selectedItem: { id: number, name: string }) {
    this.selectedEm.emit(selectedItem);
    this.selectedItem = selectedItem;
    this.onChangeDropDown();
  }

  //? 열고 닫기
  onChangeDropDown() {
    this.openDropdown = !this.openDropdown
  }
}
