import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { TagThemeSystemDirective } from '../../directives/tag.theme.directive';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    'class': 'flex items-center justify-center px-3 font-semibold py-1.5 text-xs leading-4 rounded-md'
  },
  hostDirectives: [
    {
      directive: TagThemeSystemDirective,
      inputs: ['active', 'color']
    },
  ]
})
export class TagComponent { }
