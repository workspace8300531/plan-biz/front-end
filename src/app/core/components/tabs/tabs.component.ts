import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MasterTabItemList, TabItemList } from './tab.value';
import { Roles } from 'src/interface';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  standalone: true,
  imports: [
    RouterModule,
  ]
})
export class TabsComponent {
  @Input() role?: Roles;
  //! 값 그냥 넣어주면 ROLE 값 변해도 변경되지 않음 => 해결방안 get사용!
  // tabItemList = this.role === Roles.GUEST ? TabItemList : MasterTabItemList;
  get tabItemList() {
    return this.role === Roles.GUEST ? TabItemList : MasterTabItemList;
  }

}
