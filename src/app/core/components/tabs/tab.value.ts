export const TabItemList = [
    {
        name: '마일스톤',
        path: 'milestones'
    },
    {
        name: '칸반보드',
        path: 'kanbans'
    },
    {
        name: '이슈',
        path: 'channels'
    },
    {
        name: '태그',
        path: 'labels'
    },
    {
        name: '멤버',
        path: 'members'
    }
];

export const MasterTabItemList = [
    {
        name: '마일스톤',
        path: 'milestones'
    },
    {
        name: '칸반보드',
        path: 'kanbans'
    },
    {
        name: '이슈',
        path: 'channels'
    },
    {
        name: '태그',
        path: 'labels'
    },
    {
        name: '멤버',
        path: 'members'
    },
    {
        name: '설정',
        path: 'settings'
    }
];