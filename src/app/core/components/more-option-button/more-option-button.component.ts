import { CommonModule } from "@angular/common";
import { Component, EventEmitter, Input, Output } from "@angular/core";
import { ClickOutsideDirective } from "../../directives/click-outside.directive";

@Component({
  selector: 'app-more-option-button',
  templateUrl: './more-option-button.component.html',
  standalone: true,
  imports: [
    CommonModule,
    ClickOutsideDirective
  ]
})
export class MoreOptionButtonComponent {
  @Input() type: string = "updateAndDelete" //exile

  @Output() updateBtnClick = new EventEmitter<void>();
  @Output() deleteBtnClick = new EventEmitter<void>();

  isShowOptionMenu = false;

  toggleOptionMenu(event: Event) {
    event.stopPropagation();
    this.isShowOptionMenu = !this.isShowOptionMenu;
  }

  update() {
    this.updateBtnClick.emit();
  }

  delete() {
    this.deleteBtnClick.emit();
  }
}
