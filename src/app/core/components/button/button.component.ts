import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ButtonThemeSystemDirective } from '../../directives/button.theme.directive';
import { CanWidthDirective } from '../../directives/width.directive';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    'class': 'flex items-center justify-center py-1.5 px-2.5 text-sm font-semibold leading-6 cursor-pointer rounded-md'
  },
  hostDirectives: [
    {
      directive: ButtonThemeSystemDirective,
      inputs: ['fill', 'color']
    },
    {
      directive: CanWidthDirective,
      inputs: ['w']
    },
  ]
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() { }

}
