import { Component, EventEmitter, Input, Output, forwardRef, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { tap } from 'rxjs';
import { fadeInOut } from '../../animations/fade.animation';
import { LabelComponent } from '../label/label.component';

@Component({
  selector: 'app-search-select-box',
  templateUrl: './select-box.component.html',
  standalone: true,
  imports: [
    LabelComponent,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchSelectBoxComponent),
      multi: true
    }
  ],

  animations: [fadeInOut]
})
export class SearchSelectBoxComponent {
  @Input() name = "태그"
  @Input() selectedLabels: { id: number, name: string }[] = []
  @Input() labels: { id: number, name: string }[] = []
  @Output() inputEm = new EventEmitter<string>();
  @Output() selectedEm = new EventEmitter<{ id: number, name: string }[]>();
  readonly #fb = inject(FormBuilder);
  readonly formGroup = this.#fb.group({
    name: ['']
  })
  isOpen = false

  ngOnInit() {
    this.formGroup.get('name')?.valueChanges.pipe(
      tap((res) => {
        this.inputEm.emit(res ?? "");
      })
    ).subscribe()
  }

  isSelectedLabel(label: { id: number, name: string }) {
    return this.selectedLabels.some(res => res.id === label.id);
  }

  //? select box 열고 닫기
  onChangeOpen() {
    this.isOpen = !this.isOpen;
  }

  onDeleteLabel(label: { id: number, name: string }) {
    this.selectedLabels = this.selectedLabels.filter(res => res.id !== label.id);
    this.selectedEm.emit(this.selectedLabels);
  }

  onAddLabel(label: { id: number, name: string }) {
    this.selectedLabels.push(label);
    this.selectedEm.emit(this.selectedLabels);
    this.isOpen = false;
  }
}
