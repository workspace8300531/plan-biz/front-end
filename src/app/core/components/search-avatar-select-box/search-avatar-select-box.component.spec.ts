import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SearchAvatarSelectBoxComponent } from './search-avatar-select-box.component';

describe('SearchAvatarSelectBoxComponent', () => {
  let component: SearchAvatarSelectBoxComponent;
  let fixture: ComponentFixture<SearchAvatarSelectBoxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchAvatarSelectBoxComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchAvatarSelectBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
