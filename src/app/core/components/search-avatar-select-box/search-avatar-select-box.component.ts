import { Component, EventEmitter, Input, Output, forwardRef, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import { IonAvatar } from "@ionic/angular/standalone";
import { tap } from 'rxjs';
import { IGetProfileRes } from 'src/interface';
import { IGetParticipantRes } from 'src/interface/participant/participant.response';
import { fadeInOut } from '../../animations/fade.animation';
import { LabelComponent } from '../label/label.component';

@Component({
  selector: 'app-search-avatar-select-box',
  templateUrl: './search-avatar-select-box.component.html',
  standalone: true,
  imports: [
    IonAvatar,
    LabelComponent,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchAvatarSelectBoxComponent),
      multi: true
    }
  ],

  animations: [fadeInOut]
})
export class SearchAvatarSelectBoxComponent {
  @Input() name = "태그"
  @Input() selectedLabels: IGetProfileRes[] = []
  @Input() labels: IGetParticipantRes[] = []
  @Output() inputEm = new EventEmitter<string>();
  @Output() selectedEm = new EventEmitter<IGetProfileRes[]>();
  readonly #fb = inject(FormBuilder);
  readonly formGroup = this.#fb.group({
    name: ['']
  })
  isOpen = false

  ngOnInit() {
    this.formGroup.get('name')?.valueChanges.pipe(
      tap((res) => {
        this.inputEm.emit(res ?? "");
      })
    ).subscribe();
  }

  isSelectedLabel(label: IGetProfileRes) {
    return this.selectedLabels.some(res => res.id === label.id);
  }

  //? select box 열고 닫기
  onChangeOpen() {
    this.isOpen = !this.isOpen;
  }

  onDeleteLabel(label: IGetProfileRes) {
    this.selectedLabels = this.selectedLabels.filter(res => res.id !== label.id);
    this.selectedEm.emit(this.selectedLabels);
  }

  onAddLabel(label: IGetProfileRes) {
    this.selectedLabels.push(label);
    this.selectedEm.emit(this.selectedLabels);
    this.isOpen = false;
  }
}
