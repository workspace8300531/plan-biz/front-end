import { CommonModule } from '@angular/common';
import { Component, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-input-label',
  templateUrl: './input-label.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    class: 'flex text-sm font-medium flex-col'
  },

})
export class InputLabelComponent {
  @Input() color = 'primary'
  @Input() message = ''
  @Input() labelText = ''

  isFocus = false

  @HostListener('focusin', ['$event'])
  onFocusin() {
    this.isFocus = true;
  }

  @HostListener('focusout', ['$event'])
  onFocusout() {
    this.isFocus = false;
  }
}
