import { CommonModule } from '@angular/common';
import { Component, HostListener, Input } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    class: 'flex text-sm font-medium flex-col'
  },

})
export class InputComponent {
  @Input() color = 'primary'
  @Input() message = ''

  isFocus = false

  @HostListener('focusin', ['$event'])
  onFocusin() {
    this.isFocus = true;
  }

  @HostListener('focusout', ['$event'])
  onFocusout() {
    this.isFocus = false;
  }
}
