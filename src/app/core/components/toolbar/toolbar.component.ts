import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { DialogService } from 'src/app/service/dialog.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    'class': 'flex  h-16 items-center border-b border-[#E5E7EB] w-full'
  },
})
export class ToolbarComponent {
  readonly #dialogService = inject(DialogService);

  closeModal() {
    this.#dialogService.dismissModal();
  }
}