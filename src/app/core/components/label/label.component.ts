import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    'class': 'flex items-center justify-center px-2 py-1.5 text-xs text-[#4B5563] bg-[#F9FAFB] border border-[#6B72801A] leading-4 rounded-md'
  },
})
export class LabelComponent { }