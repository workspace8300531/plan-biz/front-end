import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { StatusTagThemeSystemDirective } from '../../directives/status.tag.theme.directive';

@Component({
  selector: 'app-status-tag',
  templateUrl: './status-tag.component.html',
  standalone: true,
  imports: [CommonModule],
  host: {
    'class': 'flex items-center justify-center px-3 border font-semibold py-1.5 text-xs leading-4 rounded-full'
  },
  hostDirectives: [
    {
      directive: StatusTagThemeSystemDirective,
      inputs: ['active', 'color']
    },
  ]
})
export class StatusTagComponent { }

