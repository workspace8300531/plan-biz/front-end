import { Directive, HostBinding, Input } from "@angular/core";

@Directive({
    selector: '[w]',
    standalone: true
})
export class CanWidthDirective {
    @Input() w?: number | 'full' | 'auto';

    @HostBinding('style.width')
    get computedWidth(): string {
        if (this.w === 'auto') {
            return 'max-content';
        }
        if (this.w === 'full') {
            return '100%';
        }
        return `${this.w}px`;
    }
}
