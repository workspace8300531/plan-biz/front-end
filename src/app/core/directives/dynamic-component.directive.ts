import { Directive, Input, OnChanges, Renderer2, SimpleChanges, ViewContainerRef } from "@angular/core";

@Directive({
  selector: '[dynamicComponentDirective]',
  standalone: true
})
export class DynamicComponentDirective implements OnChanges {
  @Input('dynamicComponentDirective') component: any;
  @Input() data: any;

  constructor(
    private readonly viewContainerRef: ViewContainerRef,
    private readonly renderer: Renderer2,
  ) { }


  ngOnChanges(changes: SimpleChanges): void {
    if (!this.component) {
      this.viewContainerRef.clear();
      return;
    }

    const componentRef = this.viewContainerRef.createComponent(this.component);

    const dynamicComponentInstance = componentRef.instance as any;
    this.renderer.addClass(componentRef.location.nativeElement, "ion-page")

    for (const prop in this.data) {
      if (this.data.hasOwnProperty(prop)) {
        dynamicComponentInstance[prop] = this.data[prop];
      }
    }
  }
} 