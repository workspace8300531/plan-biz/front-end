import { Directive, HostBinding, Input } from "@angular/core";


export type ColorKit = 'papule' | 'blue' | 'yellow' | 'grey'

@Directive({
    selector: '[appThemeSystem]',
    standalone: true
})
export class StatusTagThemeSystemDirective {
    @Input() active = false;
    @Input() color: ColorKit = 'papule';

    @HostBinding('class')
    protected get computedHostClasses(): string {

        let classes = '';

        switch (this.color) {
            case 'yellow':
                classes += 'border-[#FFEED4] text-[#7B4300]';
                break;
            case 'papule':
                classes += 'border-[#E5E6FF] text-[#4F46E5]';
                break;
            case 'blue':
                classes += 'border-[#DFECFF] text-[#006BE8]';
                break;
            case 'grey':
                classes += 'border-[#E5E7EB] text-[#374151]';
                break;

        }
        return classes;
    }
}