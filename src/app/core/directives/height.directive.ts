import { Directive, HostBinding, Input } from "@angular/core";

@Directive({
    selector: '[h]',
    standalone: true
})
export class HeightDirective {
    @Input() h?: number | 'full' | 'screen';
    @Input() type: 'remainder' | 'basic' = 'basic';

    @HostBinding('style.height')
    get computedHeight(): string {
        if (this.type === 'basic') {
            if (!this.h) {
                return 'auto';
            }
            if (this.h === 'full') {
                return '100%';
            }
            if (this.h === 'screen') {
                return '100vh';
            }
            return `${this.h}px`;
        }
        else {
            return `calc(100% - ${this.h}px)`;
        }
    }
}

