import { Directive, HostBinding, Input } from "@angular/core";


export type ColorKit = 'red' | 'blue' | 'grey'

@Directive({
    selector: '[appThemeSystem]',
    standalone: true
})
export class TagThemeSystemDirective {
    @Input() active = false;
    @Input() color: ColorKit = 'red';

    @HostBinding('class')
    protected get computedHostClasses(): string {

        let classes = '';

        if (this.active) {
            classes += 'border ';
            switch (this.color) {
                case 'red':
                    classes += 'border-[#DC2626] text-[#DC2626] bg-[#FEE2E2]';
                    break;
                case 'blue':
                    classes += 'border-[#2563EB] text-[#2563EB] bg-[#DBEAFE]';
                    break;
                case 'grey':
                    classes += 'border-[#4B5563] text-[#4B5563] bg-[#F3F4F6]';
                    break;
            }
        } else {
            switch (this.color) {
                case 'red':
                    classes += 'text-[#DC2626] bg-[#FEE2E2]';
                    break;
                case 'blue':
                    classes += 'text-[#2563EB] bg-[#DBEAFE]';
                    break;
                case 'grey':
                    classes += 'text-[#4B5563] bg-[#F3F4F6]';
                    break;
            }
        }

        return classes;
    }
}