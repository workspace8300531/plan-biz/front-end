import { Directive, HostBinding, Input } from "@angular/core";

export type AppearanceType = 'solid' | 'outline'
export type ColorKit = 'primary' | 'neutral70' | 'error'

@Directive({
    selector: '[appThemeSystem]',
    standalone: true
})
export class ButtonThemeSystemDirective {
    @Input() fill: AppearanceType = 'solid';
    @Input() color: ColorKit = 'primary';

    @HostBinding('class')
    protected get computedHostClasses() {
        if (this.fill === 'outline') {
            return {
                'border': true,
                [`border-${this.color}`]: true,
                [`text-${this.color}`]: true,
                [`bg-white`]: true,
            }
        }
        else {
            return {
                [`text-white`]: true,
                [`bg-${this.color}`]: true,
            }
        }
    }
}
