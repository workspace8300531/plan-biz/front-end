import { Component, Input, WritableSignal, inject, signal } from '@angular/core';
import { AbstractControl, COMPOSITION_BUFFER_MODE, FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { catchError, map, of, tap } from 'rxjs';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { DialogService } from 'src/app/service/dialog.service';
import { LabelService } from 'src/app/service/label/label.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetLabelRes } from 'src/interface';

@Component({
  selector: 'app-label-register',
  templateUrl: './label-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    LabelComponent,
    FormsModule,
    ReactiveFormsModule,
    ToolbarComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
//? 라벨 생성 및 수정 컨테이너
export class LabelRegisterContainer {
  @Input() labelId!: number
  readonly #labelService = inject(LabelService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService)
  readonly #fb = inject(FormBuilder);
  readonly labels: WritableSignal<IGetLabelRes[]> = signal([]);
  readonly #projectId = this.#projectService.projectId;
  readonly formGroup = this.#fb.group({
    name: ['', {
      validators: [Validators.required],
      asyncValidators: [this.labelValidator.bind(this)]
    }],
  })

  ngOnInit() {
    //* 라벨 조회
    //* 기존 라벨과 상태를 공유하지 않음! 1회성
    this.#labelService.getLabelsOnce(this.#projectId()).pipe(
      tap((res) => {
        this.labels.set(res)
      })
    ).subscribe();
  }

  //? 비동기 Validators를 통해 라벨이름이 존재하는지 유효성 체크
  private labelValidator(control: AbstractControl) {
    return this.#labelService.checkName(this.#projectId(), control.value).pipe(
      map(() => {
        return null;
      }),
      catchError(() => of({ invalidLabelName: true }))
    );
  }

  //? 라벨 생성
  createLabel() {
    this.#labelService.create(this.formGroup, this.#projectId()).subscribe();
    this.#dialogService.dismissModal();
  }

  //? 라벨 수정
  updateLabel() {
    this.#labelService.update(this.formGroup, this.#projectId(), this.labelId).subscribe();
    this.#dialogService.dismissModal();
  }

  //? 라벨 삭제
  async labelDelete() {
    this.#labelService.delete(this.#projectId(), this.labelId).subscribe();
    this.#dialogService.dismissModal();
  }
}
