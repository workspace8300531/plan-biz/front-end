import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { FormArray, FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { SearchSelectBoxComponent } from 'src/app/core/components/search-select-box/select-box.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { KanbanCardComponent } from 'src/app/pages/project/workspace/kanban/kanban-card/kanban-card.component';
import { DialogService } from 'src/app/service/dialog.service';
import { LabelService } from 'src/app/service/label/label.service';
import { MilestoneService } from 'src/app/service/milestone/milestone.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetMilestoneDetailRes, Priority } from 'src/interface';

@Component({
  selector: 'app-milestone-register',
  templateUrl: './milestone-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    KanbanCardComponent,
    SearchSelectBoxComponent,
    TagComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ToolbarComponent
  ],
})
//? 마일스톤 생성 및 수정 컨테이너
export class MilestoneRegisterContainer {
  @Input() milestoneId!: number;
  @Input() onDelete!: Function
  readonly priority = Priority
  readonly #fb = inject(FormBuilder);
  readonly #milestoneService = inject(MilestoneService);
  readonly #projectService = inject(ProjectService);
  readonly #labelService = inject(LabelService);
  readonly #dialogService = inject(DialogService);
  readonly #projectId = this.#projectService.projectId;
  readonly labels = this.#labelService.labels;
  milestone?: IGetMilestoneDetailRes

  readonly formGroup = this.#fb.group({
    name: ['', {
      validators: [Validators.required]
    }],
    content: ['', {
      validators: [Validators.required]
    }],
    priority: ['', {
      validators: [Validators.required]
    }],
    labels: this.#fb.array([])
  });

  ngOnInit() {
    //* 라벨 조회
    this.#labelService.getLabels(this.#projectId()).subscribe();

    //* 마일스톤 조회
    if (!this.milestoneId) {
      return
    }

    this.#milestoneService.getMilestone(this.#projectId(), this.milestoneId).subscribe((res) => {
      console.log(res)
      this.milestone = res;
      this.formGroup.patchValue(res);
      res.milestoneLabels.forEach(label => this.labelsValue.push(this.#fb.control(label)));
    })
  }

  //? priority 토글
  onChangePriority(priority: Priority) {
    this.formGroup.patchValue({ priority })
  }

  //? 마일스톤 생성
  createMilestone() {
    this.#milestoneService.create(this.formGroup, this.#projectId()).subscribe()
    this.#dialogService.dismissModal();
  }

  //? 마일스톤 수정
  updateMilestone() {
    this.#milestoneService.update(this.formGroup, this.#projectId(), this.milestoneId).subscribe()
    this.#dialogService.dismissModal();
  }

  //? 마일스톤 삭제
  deleteKanban() {
    this.onDelete();
    this.#dialogService.dismissModal();
  }

  //? 라벨 검색
  searchTag(name: string) {
    this.#labelService.searchLabels(this.#projectId(), name).subscribe();
  }

  //? 선택된 라벨 변경
  onChangeLabelSelected(labels: { id: number, name: string }[]) {
    this.labelsValue.clear();
    labels.forEach(label => this.labelsValue.push(this.#fb.control(label)));
  }

  get labelsValue(): FormArray {
    return this.formGroup.get('labels') as FormArray;
  }
}