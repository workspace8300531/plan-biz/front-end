import { Component, Input, OnInit, inject } from '@angular/core';
import { AbstractControl, COMPOSITION_BUFFER_MODE, FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { catchError, map, of } from 'rxjs';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { DialogService } from 'src/app/service/dialog.service';
import { ProjectService } from 'src/app/service/project/project.service';


@Component({
  selector: 'app-channel-register',
  templateUrl: './channel-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    LabelComponent,
    FormsModule,
    ReactiveFormsModule,
    ToolbarComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class ChannelRegisterContainer {
  @Input() channelId!: number
  readonly #channelService = inject(ChannelService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  readonly #fb = inject(FormBuilder);
  readonly #projectId = this.#projectService.projectId;
  readonly formGroup = this.#fb.group({
    name: ['', {
      validators: [Validators.required],
      asyncValidators: [this.channelValidator.bind(this)]
    }],
  })

  //? 비동기 Validators를 통해 라벨이름이 존재하는지 유효성 체크
  private channelValidator(control: AbstractControl) {
    return this.#channelService.checkName(this.#projectId(), control.value).pipe(
      map(() => {
        return null;
      }),
      catchError(() => of({ invalidChannelName: true }))
    );
  }

  //? 채널 생성
  createChannel() {
    this.#channelService.create(this.formGroup, this.#projectId()).subscribe();
    this.#dialogService.dismissModal();
  }

  //? 채널 수정
  updateChannel() {
    this.#channelService.update(this.formGroup, this.#projectId(), this.channelId).subscribe();
    this.#dialogService.dismissModal();
  }

  //? 채널 삭제
  async labelDelete() {
    this.#channelService.delete(this.#projectId(), this.channelId).subscribe();
    this.#dialogService.dismissModal();
  }

  //? 모달 닫기
  close() {
    this.#dialogService.dismissModal();
  }
}
