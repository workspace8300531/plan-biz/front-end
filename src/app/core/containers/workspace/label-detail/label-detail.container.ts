import { Component, Input, OnInit, inject } from '@angular/core';
import { tap } from 'rxjs';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { LabelService } from 'src/app/service/label/label.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetLabelDetailRes } from 'src/interface';
import { Dropdowns } from './label.drop.down.type';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';

@Component({
  selector: 'app-label-detail',
  templateUrl: './label-detail.container.html',
  standalone: true,
  imports: [
    CommonModule,
    LabelComponent,
    ToolbarComponent
  ],
})
export class LabelDetailContainer implements OnInit {
  @Input() labelId!: number;
  readonly #projectService = inject(ProjectService);
  readonly #labelService = inject(LabelService);
  readonly #projectId = this.#projectService.projectId;
  label?: IGetLabelDetailRes;
  dropdowns = Dropdowns;

  ngOnInit() {
    //* 라벨 상세 조회
    this.#labelService.getLabel(this.#projectId(), this.labelId).pipe(
      tap((res) => {
        this.label = res
      })
    ).subscribe();
  }

  //? 드롭다운 토글
  toggleDropdown(index: number) {
    this.dropdowns[index].showDropdown = !this.dropdowns[index].showDropdown;
  }
}
