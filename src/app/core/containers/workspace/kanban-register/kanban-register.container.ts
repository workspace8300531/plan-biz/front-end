import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { FormArray, FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { tap } from 'rxjs';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { SearchAvatarSelectBoxComponent } from 'src/app/core/components/search-avatar-select-box/search-avatar-select-box.component';
import { SearchSelectBoxComponent } from 'src/app/core/components/search-select-box/select-box.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { KanbanCardComponent } from 'src/app/pages/project/workspace/kanban/kanban-card/kanban-card.component';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { DialogService } from 'src/app/service/dialog.service';
import { IssueService } from 'src/app/service/issue/issue.service';
import { KanbanService } from 'src/app/service/kanban/kanban.service';
import { LabelService } from 'src/app/service/label/label.service';
import { MilestoneService } from 'src/app/service/milestone/milestone.service';
import { ParticipantService } from 'src/app/service/participant/participant.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetKanbanDetailRes, IGetMilestoneDetailRes, IGetProfileRes, Priority, Status, StatusOptions } from 'src/interface';
import { IGetParticipantRes } from 'src/interface/participant/participant.response';


@Component({
  selector: 'app-kanban-register',
  templateUrl: './kanban-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    KanbanCardComponent,
    SearchSelectBoxComponent,
    SearchAvatarSelectBoxComponent,
    TagComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ToolbarComponent
  ],
})
export class KanbanRegisterContainer {
  @Input() kanbanId!: number;
  @Input() statusValue!: Status
  @Input() onDelete!: Function
  readonly priority = Priority;
  readonly status = Status;
  readonly statusOptions = StatusOptions;
  readonly #fb = inject(FormBuilder);
  readonly #milestoneService = inject(MilestoneService);
  readonly #kanbanService = inject(KanbanService);
  readonly #issueService = inject(IssueService);
  readonly #projectService = inject(ProjectService);
  readonly #labelService = inject(LabelService);
  readonly #channelService = inject(ChannelService);
  readonly #participantService = inject(ParticipantService);
  readonly #dialogService = inject(DialogService);
  readonly #projectId = this.#projectService.projectId;
  readonly #channel = this.#channelService.channel;
  readonly labels = this.#labelService.labels;
  readonly milestones = this.#milestoneService.milestones;
  readonly issues = this.#issueService.issues;
  inspectors: IGetParticipantRes[] = []
  workers: IGetParticipantRes[] = []
  kanban?: IGetKanbanDetailRes;

  readonly formGroup = this.#fb.group({
    name: ['', {
      validators: [Validators.required]
    }],
    priority: ['', {
      validators: [Validators.required]
    }],
    status: ['', {
      validators: [Validators.required]
    }],
    labels: this.#fb.array([]),
    milestones: this.#fb.array([]),
    issues: this.#fb.array([]),
    inspectors: this.#fb.array([], Validators.required),
    workers: this.#fb.array([], Validators.required),
  });

  ngOnInit() {
    //* status 값 변경
    this.onChangeStatus(this.statusValue)

    //* 라벨 조회
    this.#labelService.getLabels(this.#projectId()).subscribe();

    //* 마일스톤 조회
    this.#milestoneService.getMilestones(this.#projectId()).subscribe();

    //* 참여 회원 조회 
    this.#participantService.getParticipants(this.#projectId()).pipe(
      tap((res) => {
        this.inspectors = res
        this.workers = res
      })
    ).subscribe();

    //* 이슈 전체 조회 
    this.#issueService.searchIssues("").subscribe();

    //* 칸반 조회
    if (!this.kanbanId) {
      return
    }

    this.#kanbanService.getKanban(this.#projectId(), this.kanbanId).subscribe((res) => {
      this.kanban = res;
      this.formGroup.patchValue(res);
      res.kanbanLabels.forEach(label => this.labelsValue.push(this.#fb.control(label)));
      res.kanbanMilestones.forEach(milestone => this.milestonesValue.push(this.#fb.control(milestone)));
      res.kanbanIssues.forEach(issue => this.issuesValue.push(this.#fb.control(issue)));
      res.inspectors.forEach(inspector => this.inspectorsValue.push(this.#fb.control(inspector)));
      res.workers.forEach(worker => this.workersValue.push(this.#fb.control(worker)));
    })
  }

  //? priority 토글
  onChangePriority(priority: Priority) {
    this.formGroup.patchValue({ priority })
  }

  //? 칸반 생성
  createKanban() {
    this.#kanbanService.create(this.formGroup, this.#projectId()).subscribe()
    this.#dialogService.dismissModal();
  }

  //? 칸반 삭제
  deleteKanban() {
    this.onDelete();
    this.#dialogService.dismissModal();
  }

  //? 칸반 수정
  updateKanban() {
    this.#kanbanService.update(this.formGroup, this.#projectId(), this.kanbanId).subscribe()
    this.#dialogService.dismissModal();
  }

  //? 상태 변경
  onChangeStatus(status: Status) {
    this.formGroup.patchValue({ status })
  }

  //? 라벨 검색
  searchTag(name: string) {
    this.#labelService.searchLabels(this.#projectId(), name).subscribe();
  }

  //? 마일스톤 검색
  searchMilestone(name: string) {
    this.#milestoneService.searchMilestones(this.#projectId(), name).subscribe();
  }

  //? 이슈 검색
  searchIssue(name: string) {
    this.#issueService.searchIssues(name).subscribe();
  }

  //? 참여자 검색
  searchInspectors(name: string) {
    this.#participantService.searchParticipants(this.#projectId(), name).pipe(
      tap((res) => {
        this.inspectors = res
      })
    ).subscribe();
  }

  searchWorkers(name: string) {
    this.#participantService.searchParticipants(this.#projectId(), name).pipe(
      tap((res) => {
        this.workers = res
      })
    ).subscribe();
  }

  //? 선택된 라벨 변경
  onChangeLabelSelected(labels: { id: number, name: string }[]) {
    this.labelsValue.clear();
    labels.forEach(label => this.labelsValue.push(this.#fb.control(label)));
  }

  //? 선택된 마일스톤 변경
  onChangeMilestoneSelected(labels: { id: number, name: string }[]) {
    this.milestonesValue.clear();
    labels.forEach(label => this.milestonesValue.push(this.#fb.control(label)));
  }

  //? 선택된 이슈 변경
  onChangeIssueSelected(labels: { id: number, name: string }[]) {
    this.issuesValue.clear();
    labels.forEach(label => this.issuesValue.push(this.#fb.control(label)));
  }

  //? 선택된 담당자 변경
  onChangeInspectorSelected(labels: IGetProfileRes[]) {
    this.inspectorsValue.clear();
    labels.forEach(label => this.inspectorsValue.push(this.#fb.control(label)));
  }

  //? 선택된 참여자 변경
  onChangeWorkerSelected(labels: IGetProfileRes[]) {
    this.workersValue.clear();
    labels.forEach(label => this.workersValue.push(this.#fb.control(label)));
  }

  get labelsValue(): FormArray {
    return this.formGroup.get('labels') as FormArray;
  }

  get milestonesValue(): FormArray {
    return this.formGroup.get('milestones') as FormArray;
  }

  get issuesValue(): FormArray {
    return this.formGroup.get('issues') as FormArray;
  }

  get inspectorsValue(): FormArray {
    return this.formGroup.get('inspectors') as FormArray;
  }

  get workersValue(): FormArray {
    return this.formGroup.get('workers') as FormArray;
  }
}
