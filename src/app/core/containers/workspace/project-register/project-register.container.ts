import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { DialogService } from 'src/app/service/dialog.service';
import { ProjectService } from 'src/app/service/project/project.service';

@Component({
  selector: 'app-project-register',
  templateUrl: './project-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ]
})
export class ProjectRegisterContainer {
  //? 프로젝트가 존재하면 - true
  //? 프로젝트가 존재하지 않으면 - false
  @Input() isProject = false;
  readonly #fb = inject(FormBuilder);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  formGroup: FormGroup = this.#fb.group({
    name: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(12)]),
  })

  onSubmit() {
    this.#projectService.create(this.formGroup).subscribe(() => {
      this.#dialogService.dismissModal();
    });
  }
}
