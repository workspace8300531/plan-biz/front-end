import { Component, Input, OnInit, inject } from '@angular/core';
import { tap } from 'rxjs';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { DialogService } from 'src/app/service/dialog.service';
import { InvitationService } from 'src/app/service/invitation/invitation.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetProjectRes } from 'src/interface';

@Component({
  selector: 'app-invitation-accept',
  templateUrl: './invitation-accept.container.html',
  styleUrls: ['./invitation-accept.container.scss'],
  standalone: true,
  imports: [
    ButtonComponent,
  ]
})
export class InvitationAcceptContainer {
  @Input() projectId!: number
  readonly #invitationService = inject(InvitationService);
  readonly #projectService = inject(ProjectService);
  readonly #dialogService = inject(DialogService);
  project?: IGetProjectRes

  ngOnInit() {
    //* 프로젝트 상세 조회
    this.#projectService.getProject(this.projectId).pipe(
      tap((res) => {
        this.project = res;
      })
    ).subscribe()
  }

  //? 프로젝트 초대 수락하기
  onAccept() {
    this.#invitationService.updateInvitation(this.projectId).subscribe(() => {
      this.#dialogService.dismissModal()
    })
  }
}
