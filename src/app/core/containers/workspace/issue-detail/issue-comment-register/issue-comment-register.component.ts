import { CommonModule } from '@angular/common';
import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild, ViewChildren, ViewContainerRef, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { IonAvatar } from "@ionic/angular/standalone";
import { MentionModule } from 'angular-mentions';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { IssueService } from 'src/app/service/issue/issue.service';
import { ParticipantService } from 'src/app/service/participant/participant.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { UserService } from 'src/app/service/user/user.service';
import { IGetIssueCommentsRes, IGetUserProfile } from 'src/interface';


@Component({
  selector: 'app-issue-comment-register',
  templateUrl: './issue-comment-register.component.html',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    CommonModule,
    ButtonComponent,
    IonAvatar,
    MoreOptionButtonComponent,
    MentionModule
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class IssueCommentRegisterComponent {
  @Input() formGroup!: FormGroup
  @Output() submitEv = new EventEmitter()
  @ViewChild('textContainer') textContainer?: ElementRef;
  readonly #userService = inject(UserService)
  readonly #projectService = inject(ProjectService);
  readonly #participantService = inject(ParticipantService);
  readonly user = this.#userService.user;
  readonly participants = this.#participantService.participantDetails;
  readonly #projectId = this.#projectService.projectId;
  readonly formControl = new FormControl('');
  readonly temp = new FormControl('');

  ngOnInit() {
    //* 참여자 조회하기
    this.#participantService.getParticipants(this.#projectId()).subscribe();
  }

  onSelect(item: any) {
    let currentText = this.formControl.value!;
    currentText = currentText.substring(0, currentText.length - 1);

    const mentionText = `<span class="bg-primary text-sm rounded-md p-1 mx-2">@${item.username}</span>`;

    this.temp.setValue(currentText + mentionText);
    this.formControl.setValue(currentText + mentionText);
    this.formGroup.reset()
  }

  onChange(event: any) {
    if (!event.data) {
      return
    }

    this.formControl.setValue(this.temp.value + event.target.value);
  }

  //? 댓글 달기
  onSubmit() {
    this.formGroup.value.content = this.formControl.value
    this.submitEv.emit(true);
    this.formGroup.reset()
    this.formControl.reset()
    this.temp.reset()
  }

  handleKeyDown(event: KeyboardEvent): void {
    if (event.key === 'Backspace') {
      this.handleBackspace();
    }
  }

  private handleBackspace(): void {
    const contentElement = this.textContainer?.nativeElement;
    const children = contentElement.childNodes;
    const lastChild = children[children.length - 1];
    if (lastChild.nodeType === Node.TEXT_NODE) {
      let currentText = this.formControl.value!;
      currentText = currentText.substring(0, currentText.length - 1);
      this.formControl.setValue(currentText);
      this.temp.setValue(currentText)
      this.formGroup.reset()
    } else {
      contentElement.removeChild(lastChild);
      this.formControl.setValue(contentElement.innerHTML)
      this.temp.setValue(contentElement.innerHTML)
    }
  }

  formatMention(item: any): string {
    return ``;
  }
}