import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { switchMap, tap } from 'rxjs';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { IssueService } from 'src/app/service/issue/issue.service';
import { IGetIssueDetailRes } from 'src/interface';
import { IGetChannelRes } from 'src/interface/channel/channel.response';
import { IonAvatar } from "@ionic/angular/standalone";
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { IssueCommentCardComponent } from 'src/app/core/containers/workspace/issue-detail/issue-comment-card/issue-comment-card.component';
import { IssueRegisterContainer } from '../issue-register/issue-register.container';
import { IssueCommentRegisterComponent } from './issue-comment-register/issue-comment-register.component';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-issue-detail',
  templateUrl: './issue-detail.container.html',
  standalone: true,
  imports: [IonAvatar,
    CommonModule,
    LabelComponent,
    ToolbarComponent,
    MoreOptionButtonComponent,
    TagComponent,
    IssueCommentCardComponent,
    IssueCommentRegisterComponent
  ],
})
export class IssueDetailContainer implements OnInit {
  @Input() issueId!: number;
  readonly #channelService = inject(ChannelService);
  readonly #issueService = inject(IssueService);
  readonly #fb = inject(FormBuilder);
  readonly channel = this.#channelService.channel;
  issue?: IGetIssueDetailRes;
  readonly formGroup = this.#fb.group({
    content: ['', Validators.required],
    mentionUsers: this.#fb.array([]),
  })

  ngOnInit() {
    //* 이슈 상세 조회
    this.getIssueDetail().subscribe();
  }

  //? 이슈 댓글
  createIssueComment() {
    this.#issueService.createComment(this.formGroup, this.channel()!.id, this.issueId).pipe(
      switchMap(() => {
        return this.getIssueDetail()
      })
    ).subscribe();
  }

  //? 이슈 좋아요
  toggleLike() {
    this.#issueService.toggleLike(this.channel()!.id, this.issueId).pipe(
      switchMap(() => {
        return this.getIssueDetail()
      })
    ).subscribe();
  }

  //? 이슈 상세 조회
  getIssueDetail() {
    return this.#issueService.getIssue(this.channel()?.id!, this.issueId).pipe(
      tap((res) => {
        this.issue = res
      })
    )
  }
}
