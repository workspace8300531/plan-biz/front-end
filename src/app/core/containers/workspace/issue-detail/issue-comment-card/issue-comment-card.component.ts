import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { IonAvatar } from "@ionic/angular/standalone";
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { MoreOptionButtonComponent } from 'src/app/core/components/more-option-button/more-option-button.component';
import { IGetIssueCommentsRes } from 'src/interface';

@Component({
  selector: 'app-issue-comment-card',
  templateUrl: './issue-comment-card.component.html',
  standalone: true,
  imports: [
    CommonModule,
    ButtonComponent,
    IonAvatar,
    MoreOptionButtonComponent
  ],
})
export class IssueCommentCardComponent {
  @Input() issueComments!: IGetIssueCommentsRes;

  //? 이슈 댓글 삭제
  removeComment() {

  }
}