import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, inject, output } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { IonAvatar } from "@ionic/angular/standalone";
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { AuthService } from 'src/app/service/auth/auth.service';
import { DialogService } from 'src/app/service/dialog.service';
import { IGetProfileRes, IGetUserProfile } from 'src/interface';

@Component({
  selector: 'app-profile-view',
  templateUrl: './profile-view.container.html',
  standalone: true,
  imports: [
    CommonModule,
    IonAvatar,
    RouterModule,
    ButtonComponent
  ]
})
export class ProfileViewContainer {
  @Input() user!: IGetProfileRes | null
  @Output() updateEm = new EventEmitter<number>();
  readonly #authService = inject(AuthService);
  readonly #router = inject(Router);
  readonly #dialogService = inject(DialogService);

  update() {
    this.updateEm.emit(2)
  }

  logout() {
    this.#dialogService.dismissAllModals();

    this.#authService.signout().subscribe(() => {
      this.#router.navigate(['/'], { replaceUrl: true });
    })
  }
}