import { Component, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { InputComponent } from 'src/app/core/components/input/input.component';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { PASSWORD_PATTERN } from 'src/app/core/utils/patterns';
import { DialogService } from 'src/app/service/dialog.service';
import { UserService } from 'src/app/service/user/user.service';

@Component({
  selector: 'app-profile-password',
  templateUrl: './profile-password.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    LabelComponent,
    FormsModule,
    ReactiveFormsModule,
    InputComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class ProfilePasswordContainer {
  readonly #fb = inject(FormBuilder);
  readonly #userService = inject(UserService)
  readonly #dialogService = inject(DialogService)
  readonly formGroup: FormGroup = this.#fb.group({
    password: new FormControl('', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]),
    rePassword: new FormControl('', [Validators.required, Validators.pattern(PASSWORD_PATTERN)]),
  });

  onSubmit() {
    this.#userService.updatePw(this.formGroup).subscribe();
    this.#dialogService.dismissModal();
  }
}