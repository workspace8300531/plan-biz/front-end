import { Component, Input, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { EMAIL_PATTERN, PASSWORD_PATTERN } from 'src/app/core/utils/patterns';
import { IonAvatar } from "@ionic/angular/standalone";
import { DialogService } from 'src/app/service/dialog.service';
import { UserService } from 'src/app/service/user/user.service';
import { IGetProfileRes, IGetUserProfile } from 'src/interface';
import { InputComponent } from 'src/app/core/components/input/input.component';
import { Camera, CameraResultType } from '@capacitor/camera';


@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    LabelComponent,
    FormsModule,
    ReactiveFormsModule,
    IonAvatar,
    InputComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ]
})
export class ProfileUpdateContainer {
  readonly #fb = inject(FormBuilder);
  @Input() user!: IGetProfileRes | null
  readonly #userService = inject(UserService)
  readonly #dialogService = inject(DialogService)
  readonly formGroup: FormGroup = this.#fb.group({
    username: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(12)]),
    email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
    thumbnail: new FormControl(''),
    tempThumbnail: new FormControl(''),
    type: new FormControl('')
  });

  async imageUpload() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    });

    this.formGroup.patchValue({ thumbnail: image.webPath, tempThumbnail: image.webPath })
    this.formGroup.patchValue({ type: image.format })
  }

  ngOnInit() {
    if (!this.user) {
      return
    }

    this.formGroup.patchValue(this.user)
    this.formGroup.patchValue({ tempThumbnail: this.formGroup.value.thumbnail })
    this.formGroup.patchValue({ thumbnail: null })
  }

  onSubmit() {
    console.log(this.formGroup)
    this.#userService.update(this.formGroup).subscribe(() => {
      this.#dialogService.dismissModal();
    });
  }
}