import { CdkStepper, CdkStepperModule } from "@angular/cdk/stepper";
import { CommonModule } from '@angular/common';
import { Component, ViewChild, inject } from '@angular/core';
import { RouterModule } from '@angular/router';
import { IonAvatar } from "@ionic/angular/standalone";
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { ToolbarComponent } from "src/app/core/components/toolbar/toolbar.component";
import { StepperContainer } from "src/app/core/layouts/stepper/stepper.container";
import { UserService } from "src/app/service/user/user.service";
import { ProfilePasswordContainer } from "./profile-password/profile-password.container";
import { ProfileUpdateContainer } from "./profile-update/profile-update.container";
import { ProfileViewContainer } from "./profile-view/profile-view.container";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.container.html',
  standalone: true,
  imports: [
    CommonModule,
    IonAvatar,
    RouterModule,
    ButtonComponent,
    CdkStepperModule,
    ToolbarComponent,
    StepperContainer,
    ProfileViewContainer,
    ProfileUpdateContainer,
    ProfilePasswordContainer
  ],
})
export class ProfileContainer {
  @ViewChild('stepper') stepper !: CdkStepper;
  readonly #userService = inject(UserService)
  readonly user = this.#userService.user;

  async updateStepper(index: number) {
    this.stepper.selectedIndex = index;
  }

}
