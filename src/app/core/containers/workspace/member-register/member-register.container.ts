import { Component, OnInit, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { LabelComponent } from 'src/app/core/components/label/label.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { EMAIL_PATTERN } from 'src/app/core/utils/patterns';
import { DialogService } from 'src/app/service/dialog.service';
import { InvitationService } from 'src/app/service/invitation/invitation.service';
import { ProjectService } from 'src/app/service/project/project.service';

@Component({
  selector: 'app-member-register',
  templateUrl: './member-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    LabelComponent,
    FormsModule,
    ReactiveFormsModule,
    ToolbarComponent
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ]
})
export class MemberRegisterContainer {
  readonly #fb = inject(FormBuilder);
  readonly dialogService = inject(DialogService);
  readonly #invitationService = inject(InvitationService);
  readonly #projectService = inject(ProjectService);
  readonly #projectId = this.#projectService.projectId;
  emailList: string[] = [];

  formGroup: FormGroup = this.#fb.group({
    email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_PATTERN)]),
  })

  onSubmit() {
    this.emailList.map(email => {
      this.#invitationService.create(this.#projectId(), email).subscribe()
    })

    this.close()
  }

  add() {
    if (!this.formGroup.value.email || this.formGroup.get('email')?.invalid) {
      return
    }

    this.emailList.push(this.formGroup.value.email);
    this.formGroup.controls['email'].reset();
  }

  remove(index: number) {
    this.emailList.splice(index, 1);
  }

  close() {
    this.dialogService.dismissModal();
  }
}
