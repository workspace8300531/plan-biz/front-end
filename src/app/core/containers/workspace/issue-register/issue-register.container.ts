import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { FormArray, FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { SearchSelectBoxComponent } from 'src/app/core/components/search-select-box/select-box.component';
import { SelectBoxComponent } from 'src/app/core/components/select-box/select-box.component';
import { TagComponent } from 'src/app/core/components/tag/tag.component';
import { ToolbarComponent } from 'src/app/core/components/toolbar/toolbar.component';
import { KanbanCardComponent } from 'src/app/pages/project/workspace/kanban/kanban-card/kanban-card.component';
import { ChannelService } from 'src/app/service/channel/channel.service';
import { DialogService } from 'src/app/service/dialog.service';
import { IssueService } from 'src/app/service/issue/issue.service';
import { LabelService } from 'src/app/service/label/label.service';
import { ProjectService } from 'src/app/service/project/project.service';
import { IGetIssueDetailRes, Priority } from 'src/interface';

@Component({
  selector: 'app-issue-register',
  templateUrl: './issue-register.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    KanbanCardComponent,
    SearchSelectBoxComponent,
    SelectBoxComponent,
    TagComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SearchSelectBoxComponent,
    ToolbarComponent
  ],
})
export class IssueRegisterContainer {
  @Input() issueId!: number;
  readonly priority = Priority
  readonly #fb = inject(FormBuilder);
  readonly #issueService = inject(IssueService);
  readonly #channelService = inject(ChannelService);
  readonly #projectService = inject(ProjectService);
  readonly #labelService = inject(LabelService);
  readonly #dialogService = inject(DialogService);
  readonly #channel = this.#channelService.channel;
  readonly #projectId = this.#projectService.projectId;
  readonly labels = this.#labelService.labels;
  issue?: IGetIssueDetailRes

  readonly formGroup = this.#fb.group({
    name: ['', {
      validators: [Validators.required]
    }],
    content: ['', {
      validators: [Validators.required]
    }],
    priority: ['', {
      validators: [Validators.required]
    }],
    status: [false, {
      validators: [Validators.required]
    }],
    labels: this.#fb.array([])
  });

  ngOnInit() {
    //* 라벨 조회
    this.#labelService.getLabels(this.#projectId()).subscribe();

    //* 이슈 조회
    if (!this.issueId) {
      return
    }

    this.#issueService.getIssue(this.#channel()?.id!, this.issueId).subscribe((res) => {
      this.issue = res;
      this.formGroup.patchValue(res);
      res.issueLabels.forEach(label => this.labelsValue.push(this.#fb.control(label)));
    })
  }

  //? priority 토글
  onChangePriority(priority: Priority) {
    this.formGroup.patchValue({ priority })
  }

  //? 이슈 생성
  createIssue() {
    this.#issueService.create(this.formGroup, this.#channel()?.id!).subscribe();
    this.#dialogService.dismissModal();
  }

  //? 이슈 수정
  updateIssue() {
    this.#issueService.update(this.formGroup, this.#channel()?.id!, this.issueId).subscribe()
    this.#dialogService.dismissModal();
  }

  //? 라벨 검색
  searchTag(name: string) {
    this.#labelService.searchLabels(this.#projectId(), name).subscribe();
  }

  //? 선택된 라벨 변경
  onChangeLabelSelected(labels: { id: number, name: string }[]) {
    this.labelsValue.clear();
    labels.forEach(label => this.labelsValue.push(this.#fb.control(label)));
  }

  //? 선택된 상태 변경
  onChangeStatusSelected(status: { id: number, name: string }) {
    if (status.name === "열림") {
      this.formGroup.patchValue({ status: true })
    } else {
      this.formGroup.patchValue({ status: false })
    }
  }


  get labelsValue(): FormArray {
    return this.formGroup.get('labels') as FormArray;
  }
}