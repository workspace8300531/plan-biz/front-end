import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { DialogService } from 'src/app/service/dialog.service';

@Component({
  selector: 'app-action-sheet',
  templateUrl: './action-sheet.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class ActionSheetContainer {
  @Input() name!: string;
  @Input() list!: any;

  readonly #fb = inject(FormBuilder);
  readonly #dialogService = inject(DialogService);
  @Input() formGroup = this.#fb.group({
    name: [''],
  })

  ngOnInit() {
    this.formGroup.reset()
  }

  //? 선택 및 선택된 값 방출
  onSelected(selectedItem: any) {
    this.#dialogService.dismissModal(selectedItem)
  }

  onSubmit() {
    if (!this.formGroup.get('name')?.invalid) {
      this.#dialogService.dismissModal('submit')
    }
  }
}
