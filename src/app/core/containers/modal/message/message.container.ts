import { Component, Input, inject } from '@angular/core';
import { DialogService } from 'src/app/service/dialog.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.container.html',
  standalone: true,
})
export class MessageContainer {

  readonly #dialogService = inject(DialogService);

  @Input() title!: string;
  @Input() message!: string;

  closeModal() {
    this.#dialogService.dismissModal();
  }
}

