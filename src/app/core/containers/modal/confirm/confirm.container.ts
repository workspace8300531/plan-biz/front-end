import { CommonModule } from '@angular/common';
import { Component, Input, OnInit, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { DialogService } from 'src/app/service/dialog.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    CommonModule
  ]
})
export class ConfirmContainer {
  @Input() title: string = "제목";
  @Input() subTitle: string = "";
  @Input() cancelText: string = "취소";
  @Input() confirmText: string = "확인";
  @Input() confirmColor!: string
  readonly #dialogService = inject(DialogService);

  cancel() {
    this.#dialogService.dismissModal(false)
  }

  confirm() {
    this.#dialogService.dismissModal(true)
  }
}