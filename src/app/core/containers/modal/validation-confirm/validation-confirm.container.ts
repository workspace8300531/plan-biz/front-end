import { CommonModule } from '@angular/common';
import { Component, Input, inject } from '@angular/core';
import { COMPOSITION_BUFFER_MODE, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonComponent } from 'src/app/core/components/button/button.component';
import { InputLabelComponent } from 'src/app/core/components/input-label/input-label.component';
import { DialogService } from 'src/app/service/dialog.service';

@Component({
  selector: 'app-validation-confirm',
  templateUrl: './validation-confirm.container.html',
  standalone: true,
  imports: [
    InputLabelComponent,
    ButtonComponent,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: COMPOSITION_BUFFER_MODE,
      useValue: false
    }
  ],
})
export class ValidationConfirmContainer {
  @Input() title: string = "제목";
  @Input() subTitle: string = "";
  @Input() cancelText: string = "취소";
  @Input() confirmText: string = "확인";
  @Input() confirmColor!: string
  @Input() validationMessage!: string
  readonly #fb = inject(FormBuilder);
  readonly #dialogService = inject(DialogService);
  @Input() formGroup = this.#fb.group({
    name: [''],
  })

  cancel() {
    this.#dialogService.dismissModal(false)
  }

  confirm() {
    if (this.formGroup.value.name !== this.validationMessage) {
      return
    }

    this.#dialogService.dismissModal(true)
  }
}