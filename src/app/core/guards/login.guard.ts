import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";

import { TokenService } from "src/app/service/token/token.service";


export async function LoginGuard(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const tokenService = inject(TokenService);
    const router = inject(Router)
    const { accessToken } = await tokenService.getToken()

    if (accessToken) {
        router.navigate(['/projects'], { replaceUrl: true });
        return false;
    }

    return true;
}
