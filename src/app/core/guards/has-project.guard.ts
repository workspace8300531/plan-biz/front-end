import { inject } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";

import { map } from "rxjs";
import { ProjectService } from "src/app/service/project/project.service";


export function GetProjectGuard(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const projectService = inject(ProjectService);

    return projectService.getProjects()
        .pipe(map(() => {
            return true;
        }));
}
