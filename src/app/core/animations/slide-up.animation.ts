import { animate, state, style, transition, trigger } from "@angular/animations";

export const slideUp = trigger('slideUp', [
    state('void', style({ transform: 'translateY(100%)', opacity: 0 })),
    state('*', style({ transform: 'translateY(0)', opacity: 1 })),
    transition(':enter', animate('200ms ease-out')),
    transition(':leave', animate('200ms ease-in'))
]);