import { createAnimation } from '@ionic/angular';

export function LeaveAnimation(baseEl: any) {
    const root = baseEl.shadowRoot;

    const wrapperAnimation = createAnimation()
        .addElement(root.querySelector('.modal-wrapper')!)
        .keyframes([
            { offset: 0, opacity: '0.99', transform: 'translateX(0)' },
            { offset: 1, opacity: '0', transform: 'translateX(100%)' }
        ]);

    return createAnimation()
        .addElement(baseEl)
        .easing('ease-out')
        .duration(300)
        .addAnimation([wrapperAnimation]);
}
