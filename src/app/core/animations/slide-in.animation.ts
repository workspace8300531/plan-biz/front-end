import { animate, state, style, transition, trigger } from "@angular/animations";

export const slideIn = trigger('slideIn', [
    state('right', style({ transform: 'translateX(0)' })),
    state('left', style({ transform: 'translateX(0)' })),
    transition('void => left', [
        style({ transform: 'translateX(-100%)' }),
        animate('200ms ease-in', style({ transform: 'translateX(0%)' }))
    ]),
    transition('left => void', [
        animate('200ms ease-in', style({ transform: 'translateX(-100%)' }))
    ]),
    transition('void => right', [
        style({ transform: 'translateX(100%)' }),
        animate('200ms ease-in', style({ transform: 'translateX(0%)' }))
    ]),
    transition('right => void', [
        animate('200ms ease-in', style({ transform: 'translateX(100%)' }))
    ]),
]);