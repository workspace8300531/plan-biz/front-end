import { animate, style, transition, trigger } from "@angular/animations";

export const fadeWithScale = trigger('fadeWithScale', [
  transition(':enter', [
    style({ opacity: 0, transform: 'scale(0.85)' }),
    animate('200ms ease-out', style({ opacity: 1, transform: 'scale(1)' }))
  ]),
  transition(':leave', [
    animate('100ms ease-in', style({ opacity: 0, transform: 'scale(0.85)' }))
  ])
]);

