import { HttpContextToken } from "@angular/common/http";

//? 기본값 false
export const SKIP_CREDENTIAL = new HttpContextToken<boolean>(() => false);

