import { HttpContextToken, HttpEvent, HttpHandlerFn, HttpRequest } from "@angular/common/http";
import { inject } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, catchError, switchMap, throwError } from "rxjs";
import { AuthService } from "src/app/service/auth/auth.service";
import { DialogService } from "src/app/service/dialog.service";
import { TokenService } from "src/app/service/token/token.service";
import { ResponseErrorType } from "src/interface/error/error.response";
import { SKIP_CREDENTIAL } from "./skip.token";

export function CredentialInterceptor(req: HttpRequest<unknown>, next: HttpHandlerFn): Observable<HttpEvent<unknown>> {
  const tokenService = inject(TokenService);
  const authService = inject(AuthService);
  const router = inject(Router);
  const dialogService = inject(DialogService);
  const { accessToken, refreshToken } = tokenService.token();

  //? 토큰 검사 skip
  if (req.context.get(SKIP_CREDENTIAL)) {
    return next(req);
  }

  //? 토큰 header에 넣어 요청 보내기
  req = req.clone({
    setHeaders: { 'Authorization': `Bearer ${accessToken}` }
  });

  return next(req).pipe(
    catchError((res: ResponseErrorType) => {
      if (res.status === 0) {
        dialogService.showToast({
          message: '서버와 연결할 수 없습니다',
          duration: 2000,
          position: 'bottom',
          icon: 'assets/icons/check-toast.svg'
        });

        // 여기서 서버 연결 실패에 대한 에러를 다시 발생시킵니다.
        return throwError(() => new Error('서버와 연결할 수 없습니다'));
      }

      //! 토큰 리프레쉬 문제 있음 !!!!! 
      if (res.status === 401 && refreshToken) {
        return authService.refresh().pipe(
          switchMap((tokens) =>
            next(req.clone({
              setHeaders: { 'Authorization': `Bearer ${tokens.accessToken}` }
            }))
          ),
          catchError(() => {
            authService.signout().subscribe();
            router.navigateByUrl('/')
            // 로그아웃 후 관련 에러를 다시 발생시킵니다.
            return throwError(() => new Error('인증 실패: 로그아웃 처리됨'));
          })
        );
      }

      // 그 외의 경우에는 받은 에러를 그대로 다시 발생시킵니다.
      return throwError(() => res);
    })
  );
} 