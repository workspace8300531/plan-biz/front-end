import { Routes } from '@angular/router';
import { GetProjectGuard } from './core/guards/has-project.guard';
import { LoginGuard } from './core/guards/login.guard';
import { ProjectPage } from './pages/project/project.page';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'signin',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadComponent: () => import('./pages/home/home.page').then((m) => m.HomePage),
  },
  {
    path: 'signup',
    canActivate: [LoginGuard],
    loadComponent: () => import('./pages/signup/signup.page').then((m) => m.SignupPage),
  },
  {
    path: 'signin',
    canActivate: [LoginGuard],
    loadComponent: () => import('./pages/signin/signin.page').then((m) => m.SigninPage),
  },
  {
    path: 'update-password',
    canActivate: [LoginGuard],
    loadComponent: () => import('./pages/password/update-password/update-password.page').then((m) => m.UpdatePasswordPage),
  },
  {
    path: 'forgot-password',
    canActivate: [LoginGuard],
    loadComponent: () => import('./pages/password/forgot-password/forgot-password.page').then((m) => m.ForgotPasswordPage),
  },
  {
    path: 'projects',
    canActivate: [GetProjectGuard],
    component: ProjectPage,
    // loadComponent: () => import('./pages/project/project.page').then(m => m.ProjectPage),
    children: [
      {
        path: ':projectId',
        loadComponent: () => import('./pages/project/workspace/workspace.page').then(m => m.WorkspacePage),
        children: [
          {
            path: 'milestones',
            loadComponent: () => import('./pages/project/workspace/milestone/milestone.page').then(m => m.MilestonePage)
          },
          {
            path: 'kanbans',
            loadComponent: () => import('./pages/project/workspace/kanban/kanban.page').then(m => m.KanbanPage)
          },
          {
            path: 'channels',
            loadComponent: () => import('./pages/project/workspace/channel/channel.page').then(m => m.ChannelPage),
            children: [
              {
                path: ':channelId/issues',
                loadComponent: () => import('./pages/project/workspace/channel/issue/issue.page').then(m => m.IssuePage),
              }
            ]
          },
          {
            path: 'labels',
            loadComponent: () => import('./pages/project/workspace/label/label.page').then(m => m.LabelPage)
          },
          {
            path: 'members',
            loadComponent: () => import('./pages/project/workspace/member/member.page').then(m => m.MemberPage)
          },
          {
            path: 'settings',
            loadComponent: () => import('./pages/project/workspace/settings/settings.page').then(m => m.SettingsPage)
          },
        ]
      }
    ]
  }
];
