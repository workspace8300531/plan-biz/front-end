export const environment = {
  production: true,
  server: 'https://planbiz-api.hohoco.store/api/v1',
  client: 'https://planbiz.hohoco.store',
  apn: 'co.kr.planbiz',
  get deepLink() {
    return `https://planbiz.page.link/?link=${this.client}`;
  },
};
