// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  server: 'https://planbiz-api.hohoco.store/api/v1',
  client: 'https://planbiz.hohoco.store',
  apn: 'co.kr.planbiz',
  get deepLink() {
    return `https://planbiz.page.link/?link=${this.client}`;
  },
};

