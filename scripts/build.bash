# 서버 접속 정보
SERVER_KEY="scripts/hoho.pem"
SERVER_USER="hoho"
SERVER_IP="59.0.87.169"
SERVER_PATH="/opt/planbiz-client" 
# 도커 관련 정보
APP_NAME="planbiz-client"
DOCKER_FILE_PATH="."
DOCKER_FILE="Dockerfile" 
DOCKER_COMPOSE_FILE="docker-compose.yml"
SERVER_TAR_FILE="$APP_NAME.gz"

# ! Step 1: 프로젝트 빌드
echo "[프로젝트 빌드중...]"
ionic build --prod

# ! Step 2: 도커 이미지 빌드 및 tar 파일로 저장
echo "[도커 이미지 생성중...]"
docker build --platform linux/amd64 -t $APP_NAME . 

echo "[도커 이미지 파일로 추출중...]"
docker save $APP_NAME | gzip > $SERVER_TAR_FILE

# ! Step 3: 서버 작업 환경 확인 및 구성
echo "[작업환경 구성중...]"
ssh -i $SERVER_KEY $SERVER_USER@$SERVER_IP << EOF
sudo mkdir -p $SERVER_PATH
sudo chmod 777 $SERVER_PATH
EOF

# ! Step 4: 파일 전송
echo "[도커 관련 파일 전송중...]"
scp -i $SERVER_KEY $SERVER_TAR_FILE $SERVER_USER@$SERVER_IP:$SERVER_PATH/ 
scp -i $SERVER_KEY $DOCKER_FILE_PATH/$DOCKER_COMPOSE_FILE $SERVER_USER@$SERVER_IP:$SERVER_PATH/

# ! Step 5: 기존 도커 컨테이너와 이미지 관리
echo "[Docker 컨테이너 조정중...]"
ssh -i $SERVER_KEY $SERVER_USER@$SERVER_IP << EOF 
 
echo "[기존의 컨테이너가 있는지 확인중...]"
if sudo docker-compose -f $SERVER_PATH/$DOCKER_COMPOSE_FILE ps; then
    echo "[기존의 컨테이너 삭제중...]"
      sudo docker-compose -f $SERVER_PATH/$DOCKER_COMPOSE_FILE down --remove-orphans

    echo "[기존의 이미지 삭제중...]"
    sudo docker rmi $APP_NAME
fi 
 
# !Step 6: 새 Docker 이미지 로드 및 컨테이너 실행
echo "[tar 파일로부터 Docker 이미지 로드중...]"
sudo docker load -i $SERVER_PATH/$SERVER_TAR_FILE

echo "[새로운 컨테이너 실행중...]"
sudo docker-compose -f $SERVER_PATH/$DOCKER_COMPOSE_FILE up -d
EOF

echo "배포 완료가 완료되었습니다. 서버 상태를 확인해주세요."
