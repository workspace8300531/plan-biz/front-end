/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'primary': '#4F46E5',
        'primaryTint': '#6366F1',
        'error': '#DC2626',
        'placeholder': '#9CA3AF',
        'border': '#D1D5DB',
        'label': '#111827',
        'disabled': '#999999',

        'neutral100': '#ffffff',
        'neutral90': "#F7F8F8",
        'neutral80': "#EEEFF0",
        'neutral70': "#D9D9D9",
        'neutral60': "#9E9E9E",
        'neutral50': "#6B6B6B",
        'neutral40': "#4F4F4F",
        'neutral30': "#2A2A2A",
        'neutral20': "#1C1C1C",
        'neutral10': '#000000'
      }
    }
  },
  plugins: [],
}

