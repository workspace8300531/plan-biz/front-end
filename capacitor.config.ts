import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'co.kr.planbiz',
  appName: 'front-end2',
  webDir: 'www',
  server: {
    androidScheme: 'http',
  },
  ios: {
    preferredContentMode: 'mobile'
  },
};

export default config;
