# 1단계: 빌드 스테이지
# Node 이미지를 사용하여 Ionic 프로젝트 빌드
FROM node:latest as build-stage

WORKDIR /app

# package.json과 package-lock.json 파일 복사
COPY package*.json /app/

# 프로젝트 의존성 설치
RUN npm install

# 프로젝트 소스 코드 복사
COPY ./ /app/

# Ionic 프로젝트 빌드, --prod 플래그를 사용하여 프로덕션 빌드 수행
RUN npm run build --prod

# 2단계: 서비스 스테이지
# Nginx를 사용하여 애플리케이션 서비스
FROM nginx:alpine

# 빌드 스테이지에서 생성된 빌드 파일들을 Nginx 서버에 복사
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=build-stage /app/www /usr/share/nginx/html 

# Nginx의 기본 포트 80을 열어줌
EXPOSE 80

# Nginx 서버 실행
CMD ["nginx", "-g", "daemon off;"]